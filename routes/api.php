<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Authorization, Content-Type");

Route::post('login', 'API\LoginController@login');
Route::post('otp', 'API\LoginController@otp');
Route::post('resend-otp', 'API\LoginController@resendOtp');
Route::post('verify-otp', 'API\LoginController@otpVerify');
Route::post('signup', 'API\LoginController@signup');
Route::post('v1/forgot-password', 'API\LoginController@forgotPassword');	// for otp
Route::post('v2/forgot-password-save', 'API\LoginController@forgotPasswordSave');	// for saving password
Route::post('reset-password', 'API\LoginController@resetPassword');
Route::post('login-with-social', 'API\LoginController@loginWithSocial');
Route::post('v1/get-countries', 'API\LoginController@countries');
Route::get('agreement-document/{type}', 'API\LoginController@agreement_document');
Route::get('privacy-policy', 'API\LoginController@privacy_policy');
Route::get('terms-and-condition', 'API\LoginController@terms_and_condition');

Route::group(['middleware' => ['jwt.auth']], function() {
	Route::post('user-logout', 'API\LoginController@userLogout');
	Route::post('get-profile', 'API\LoginController@getProfile');
	Route::post('change-password', 'API\LoginController@changePassword');
	Route::post('update-profile-customer', 'API\LoginController@updateProfile');
	Route::post('update-profile-vendor', 'API\LoginController@updateProfileVendor');
	Route::post('update-profile-driver', 'API\LoginController@updateProfileDriver');
	Route::post('get-continentals', 'API\LoginController@getContinentals');



	//items
	Route::post('get-categories', 'API\ItemsController@getCategories');
	Route::post('get-sub-categories', 'API\ItemsController@getSubCategories');
	Route::post('add-item', 'API\ItemsController@addItem');
	Route::post('update-item', 'API\ItemsController@updateItem');
	Route::post('delete-item', 'API\ItemsController@deleteItem');
	Route::post('unavailable-item', 'API\ItemsController@unavailableItem');
	Route::post('available-item', 'API\ItemsController@availableItem');
	Route::post('my-list-item', 'API\ItemsController@myListItem');
	Route::post('my-list-category-wise-item', 'API\ItemsController@myCategoryWiseItem');
	Route::post('detail-item', 'API\ItemsController@itemDetail');
	Route::post('list-item-category-wise', 'API\ItemsController@categoryWiseItem');
	Route::post('home-page-item-list', 'API\ItemsController@HomePageListItem');
	Route::post('get-item-detail', 'API\ItemsController@getItemDetail');
	Route::post('get-restaurant-category-veg-items', 'API\ItemsController@getRestaurantCategoryVegItems');

	//Customer
	Route::post('add-address', 'API\UsersController@addAddress');
	Route::post('list-address', 'API\UsersController@getAddresses');
	Route::post('update-address', 'API\UsersController@updateAddress');
	Route::post('add-bank', 'API\UsersController@addBank');
	Route::post('list-bank', 'API\UsersController@getBank');
	Route::post('update-bank', 'API\UsersController@updateBank');
	Route::post('delete-bank', 'API\UsersController@deleteBank');
	Route::post('search-customer', 'API\UsersController@searchCustomer');
	Route::post('create-user-search', 'API\UsersController@createUserSearch');
	Route::post('recent-user-search', 'API\UsersController@getRecentUserSearch');
    Route::post('contact-sync', 'API\UsersController@contactSync');
    Route::post('app-users', 'API\UsersController@AppUsersListing');
    Route::post('get-contacts', 'API\UsersController@getContacts');
    Route::post('update-default-address','API\UsersController@updateDefaultAddress');
    Route::post('delete-address','API\UsersController@deleteAddress');
	//Restaurant
	Route::post('restaurant-detail', 'API\RestaurantsController@getRestaurant');
	Route::post('search-item-restaurant', 'API\RestaurantsController@searchItemRestaurant');
	Route::post('add-driver-partner', 'API\RestaurantsController@addDriverPartner');
	Route::post('driver-partner-list', 'API\RestaurantsController@driverPartnerList');
	Route::post('delete-driver-partner', 'API\RestaurantsController@deleteDriverPartner');
    Route::post('get-restaurant-veg-items', 'API\RestaurantsController@getRestaurantVegItems');
 	Route::post('get-user-nearby-restaurant', 'API\RestaurantsController@getNearbyRestaurants');

	//Orders
	Route::post('post-item-in-cart', 'API\OrdersController@postItemInCart');
	Route::post('clear-cart', 'API\OrdersController@clearCart');
	Route::post('cart-detail', 'API\OrdersController@cartDetail');
	Route::post('empty-cart', 'API\OrdersController@emptyCart');
	Route::post('post-order', 'API\OrdersController@postOrder');
	Route::post('change-order-status', 'API\OrdersController@changeOrderStatus');
	Route::post('order-list', 'API\OrdersController@getOrderList');
	Route::post('order-detail', 'API\OrdersController@orderDetail');
	Route::post('order-list-inprocess', 'API\OrdersController@getOrderListInProcess');
	Route::post('order-list-completed', 'API\OrdersController@getOrderListCompleted');
	Route::post('order-list-history', 'API\OrdersController@getOrderHistory');
	Route::post('order-list-request', 'API\OrdersController@getOrderListRequest');
	Route::post('change-delivery-order-status', 'API\OrdersController@changeDeliveryOrderStatus');
    Route::post('get-complete-orders','API\OrdersController@getCompleteOrders');
    Route::post('get-pending-orders','API\OrdersController@getPendingOrders');
    Route::get('order-history', 'API\OrdersController@orderHistory');
    Route::get('order-history-driver', 'API\OrdersController@orderHistoryDriver');
	Route::get('order-history-group/{group_id}', 'API\OrdersController@orderHistoryGroup');
	Route::get('order-detail/{id}', 'API\OrdersController@orderDetails');
    Route::post('user-order-decline', 'API\OrdersController@userOrderDecline');
    Route::get('my-active-order', 'API\OrdersController@myActiveOrder');
    //User Group
    Route::post('create-user-group','API\UsersController@createGroup');
    Route::post('delete-user-group','API\UsersController@deleteUserGroup');
    Route::post('user-group-list','API\UsersController@userGroupList');
    Route::post('group-detail','API\UsersController@groupDetail');

    Route::post('update-group-member-status','API\UsersController@updateGroupRequestStatus');
    Route::post('add-group-member','API\UsersController@addGroupMember');
    Route::post('delete-group-member','API\UsersController@deleteGroupMember');
    Route::post('list-group-members','API\UsersController@listGroupMembers');
    Route::post('group-invitations-list','API\UsersController@groupInvitationList');
    Route::post('check-group-status','API\UsersController@checkGroupStatus');

    Route::post('select-payment-method','API\UsersController@selectPaymentMethod');
    Route::post('payment-mode-details','API\UsersController@paymentModeDetails');
    Route::post('confirm-cod','API\UsersController@confirmCOD');
    Route::post('pay-money-to-group-admin','API\UsersController@payMoneyToGroupAdmin');

    //Poll
    Route::post('create-poll','API\PollController@create');
    Route::post('delete-poll','API\PollController@delete');
    Route::post('vote-restaurant','API\PollController@voteRestaurant');
    Route::post('poll-detail','API\PollController@pollDetail');
    Route::post('poll-result-info','API\PollController@pollResultInfo');
    Route::post('close-poll','API\PollController@closePoll');
    Route::post('update-menu-expire-time', 'API\PollController@updateMenuExpireTime');
    Route::post('group-admin-cart-details','API\OrdersController@groupAdminCartDetails');

    // Wallet
    Route::resource('wallet','API\WalletController');
    Route::get('wallet-transactions','API\WalletController@walletTransactions');
    Route::resource('restaurant-rating','API\RestaurantRatingController');
    Route::resource('restaurant-favourite','API\RestaurantFavouriteController');
    Route::resource('driver-rating','API\DriverRatingController');
    Route::get('favourite-restaurant-list','API\RestaurantFavouriteController@favouriteRestaurantList');


    // Setting Apis
	Route::post('notification-enable', 'API\UsersController@notification_enable');
    Route::get('settings', 'API\SettingController@settings');
	Route::post('cod-enable', 'API\UsersController@codEnable');

    // Wallet Api
	Route::get('customer-stripe-connection', 'API\WalletController@stripeConnection');
    Route::post('wallet-to-bank-transfer', 'API\WalletController@walletToBankTransfer');

    Route::get('notifications', 'API\NotificationController@index');

    // Dwolla Apis
    Route::post('add-customer-funding-source', 'API\WalletController@addCustomerFundingSource');
    Route::get('retrive-customer-funding-sources', 'API\WalletController@retriveCustomerFundingSources');
    Route::post('delete-customer-funding-source', 'API\WalletController@deleteCustomerFundingSource');









});
