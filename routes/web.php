<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Mail;

Route::get('/', 'Admin\DashboardController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/call', 'VoiceController@index');
Route::post('/call', 'VoiceController@initiateCall')->name('initiate_call');

Route::get('stripe', 'StripePaymentController@stripe');
Route::post('stripe', 'StripePaymentController@stripePost')->name('stripe.post');
Route::get('notification', 'API\OrdersController@testnotification');
Route::get('agreement-document/{type}', 'Admin\DashboardController@agreement_document');
Route::get('text', 'Admin\DashboardController@text');
Route::get('check-mail/{email}', 'Front\UsersController@check_mail');
Route::get('account-confirmation', 'Front\UsersController@accountConfirmation');

Route::get('/create-connect', 'Front\StripeConnectController@createConnect')->name('create-connect');
Route::get('/create-connect-success', 'Front\StripeConnectController@createConnectSuccess')->name('create-connect-success');
Route::get('privacy-policy', 'Front\UsersController@privacy_policy');
Route::get('terms-and-condition', 'Front\UsersController@terms_and_condition');

Route::get('braintree','PaymentController@checkout');
Route::post('braintree','PaymentController@checkout');
Route::get('adyen','PaymentController@adyen');

Route::get('dwolla','PaymentController@dwolla');
Route::post('dwolla-redirect','PaymentController@dwolla_redirect');
Route::get('dwolla-callback','PaymentController@dwolla_callback');


Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {

    Route::get('/dashboard', 'DashboardController@index')->name('admin.dashboard');
    Route::get('/profile', 'AdminController@profile')->name('admin.profile');
    Route::post('/update-profile', 'AdminController@updateProfile')->name('admin.update.profile');
    Route::get('/change-password', 'AdminController@changePassword')->name('admin.change.password');
    Route::post('/update-password', 'AdminController@updatePassword')->name('admin.update.password');

    Route::get('/user-list', 'UsersController@index')->name('admin.user.list');
    Route::get('/user-active/{id}', 'UsersController@active')->name('admin.user.active');
    Route::get('/user-inactive/{id}', 'UsersController@inActive')->name('admin.user.inactive');
    Route::get('/user-detail/{id}', 'UsersController@detail')->name('admin.user.detail');



    //CMS Routes
    Route::get('/cms', 'CmsController@index')->name('admin.cms.index');
    Route::get('/cms-active/{id}', 'CmsController@active')->name('admin.cms.active');
    Route::get('/cms-inactive/{id}', 'CmsController@inActive')->name('admin.cms.inactive');
    Route::get('/cms/edit/{id}', 'CmsController@edit')->name('admin.cms.edit');
    Route::get('/cms/create', 'CmsController@create')->name('admin.cms.create');
    Route::put('/cms-update/{id}', 'CmsController@update')->name('admin.cms.update');
    Route::post('/cms-store', 'CmsController@store')->name('admin.cms.store');
    Route::get('/cms-detail/{id}', 'CmsController@detail')->name('admin.cms.detail');
    Route::get('/cms-detail/{id}', 'CmsController@detail')->name('admin.cms.detail');

    // Vendors Routes
    Route::get('/vendor-list', 'VendorsController@index')->name('admin.vendor.list');
    Route::get('/vendor-active/{id}', 'VendorsController@active')->name('admin.vendor.active');
    Route::get('/vendor-inactive/{id}', 'VendorsController@inActive')->name('admin.vendor.inactive');
    Route::get('/vendor-detail/{id}', 'VendorsController@detail')->name('admin.vendor.detail');
    Route::get('/vendor-agreement/{id}', 'VendorsController@vendor_agreement')->name('admin.vendor.agreement');


    // Drivers Routes
    Route::get('/driver-list', 'DriversController@index')->name('admin.driver.list');
    Route::get('/driver-active/{id}', 'DriversController@active')->name('admin.driver.active');
    Route::get('/driver-inactive/{id}', 'DriversController@inActive')->name('admin.driver.inactive');
    Route::get('/driver-detail/{id}', 'DriversController@detail')->name('admin.driver.detail');
    Route::get('/driver-agreement/{id}', 'DriversController@driver_agreement')->name('admin.driver.agreement');

    //Categories Routes
    Route::resource('categories', 'CategoriesController');
    Route::get('categories', 'CategoriesController@index')->name('admin.categories');
    Route::put('categories/update/{id}', 'CategoriesController@update')->name('admin.categories.update');
    Route::post('categories/store', 'CategoriesController@store')->name('admin.categories.store');
    Route::get('category-list', 'CategoriesController@index')->name('admin.categories.list');
    Route::get('category-active/{id}', 'CategoriesController@active')->name('admin.categories.active');
    Route::get('category-inactive/{id}', 'CategoriesController@inActive')->name('admin.categories.inactive');

    //Items Routes
    Route::resource('items', 'ItemsController');
    Route::get('items', 'ItemsController@index')->name('admin.items');

    // Orders Routes
    Route::resource('orders', 'OrdersController');

    //User Groups

    Route::get('/user-group', 'UserGroupController@index')->name('admin.user-group');
    Route::get('user-group-list', 'UserGroupController@index')->name('admin.user-groups.list');
    Route::get('/user-group/{id}/members', 'UserGroupController@members')->name('admin.user-groups.members');
    Route::get('/user-group/{id}/polls', 'UserGroupController@polls')->name('admin.user-groups.polls');

    Route::resource('walletpercentage', 'WalletPercentageController');
    Route::get('walletpercentage-active/{id}', 'WalletPercentageController@active')->name('admin.walletpercentage.active');
    Route::get('walletpercentage-inactive/{id}', 'WalletPercentageController@inActive')->name('admin.walletpercentage.inactive');
    Route::get('/settings', 'SettingController@settings')->name('admin.settings');
    Route::post('/settings', 'SettingController@settingsUpdate')->name('admin.settings');
});

Route::get('sendmail', function () {
    $data = array(
        'bodyMessage' => "hello trilok"
    );

    Mail::send('mail', $data, function ($message) {
        $message->to('trilokkumar970@gmail.com')->subject('Just Laravel demo email using SendGrid');
    });

    return 'done ';
});
