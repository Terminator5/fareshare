const express = require('express');
var app = express();
var fs = require("fs");
var http = require('http').Server(app);
//var io = require('socket.io')(http, { pingInterval: 5000, pingTimeout: 3000, cookie: false });
var io = require('socket.io')(http);
var mysql = require('mysql');
var request = require('request');

app.use(express.static('upload_chat_imgs'));

var port = 5059;

var host_url = "https://fareshare.info:"+port;
var host_base_url = "https://fareshare.info/api/";
// var default_img = host_url + "public/img/default.jpeg";
var moment = require('moment');

var con = mysql.createConnection({
    host     : '127.0.0.1',
    user     : 'fareshare_user',
    password : 'fareshare@2021@@$',
    database : 'fareshare',
    charset  : 'utf8mb4',
    port     : '3306',
    dateStrings:true
});


var _url = host_url+"/";

/*mysql connect connection*/
con.connect(function(err) {
    if (err)
        return console.log("error"+err);
    console.log("Connected");
});

var result_res ={};
var sockets = {};


app.get('/freelancer', function(req, res) {
    res.sendFile(__dirname + '/freelancer.html');
});

app.get('/requester', function(req, res) {
    res.sendFile(__dirname + '/requester.html');
});

 /*socket update gardner location*/
io.on('connection', function(socket) {

    console.log("connected : " + socket.handshake.query.user_id);
      if (!sockets[socket.handshake.query.user_id]) {
        sockets[socket.handshake.query.user_id] = [];
      }
      sockets[socket.handshake.query.user_id].push(socket);
      if (sockets[socket.handshake.query.user_id] != undefined) {
        var sqlUpdate = "UPDATE users SET online='1' where id='" +  socket.handshake.query.user_id + "'";

        con.query(sqlUpdate, function(err, result) {
        if (err)
            return console.log(err);
        });
      }

    console.log('a user connected\n', socket.handshake.query);

    var user_id_temp = socket.handshake.query.user_id;
    // con.query("update users set is_online='Y' where id ='" + user_id_temp + "'");

    socket.on('restaurant_get_order', function (data, cb) {

        var receiver_data_arr = {'data': data, 'message': 'Receiver Data', 'status': 'true'};

        console.log('restaurant data -----',receiver_data_arr);
        var get_user = "SELECT user_id FROM `restaurants` WHERE id=" + data.restaurant_id;
        con.query(get_user, function (err, row) {
            if (err)
                console.log('blocked_by_me ', err);

            if (row.length > 0)
            {
                var other_user_id = row[0]['user_id'];
                if (sockets[other_user_id] != undefined) {

                    for (var index in sockets[other_user_id]) {
                        sockets[other_user_id][index].emit('received_order', receiver_data_arr);
                        // console.log('user_id------',data.other_user_id);
                        console.log('receiver_data_arr',receiver_data_arr);
                    }

                    //sockets[data.other_user_id].emit('receive_message', newdataarr2);
                }

            }


        });

    });

     socket.on('get_order_status', function (data, cb) {
        var receiver_data_arr = {'data': data, 'message': 'Order Status', 'status': 'true'};



        console.log('restaurant data -----',receiver_data_arr);

        if (sockets[data.user_id] != undefined) {

            for (var index in sockets[data.user_id]) {
                sockets[data.user_id][index].emit('user_order_status', receiver_data_arr);
                // console.log('user_id------',data.other_user_id);
                console.log('receiver_data_arr',receiver_data_arr);
            }

            //sockets[data.other_user_id].emit('receive_message', newdataarr2);
        }


    });

     socket.on('vote_restaurant', function (data, cb) {
        var receiver_data_arr = {'data': data, 'message': 'Votting Information', 'status': 'true'};



        console.log('restaurant data -----',receiver_data_arr);

        for(var idx in data.members){
            //  var member_id = data.members[idx]['id'];
             var member_id = data.members[idx]['user']['id'];
        console.log('member_id -----',member_id);

            if (sockets[member_id] != undefined) {
                // console.log('sockets[member_id] -----',sockets[member_id]);

            for (var index in sockets[member_id]) {
                // console.log('index -----',index);

                sockets[member_id][index].emit('receive_vote_info', receiver_data_arr);
                // console.log('user_id------',data.other_user_id);
                console.log('receiver_data_arr',receiver_data_arr);
            }

            //sockets[data.other_user_id].emit('receive_message', newdataarr2);
        }


        }


    });

     socket.on('select_items', function (data, cb) {
        var receiver_data_arr = {'data': data, 'message': 'Selected Items', 'status': 'true'};



        console.log('restaurant data -----',receiver_data_arr);

        if (sockets[data.creater_id] != undefined) {

            for (var index in sockets[data.creater_id]) {
                sockets[data.creater_id][index].emit('received_selected_items', receiver_data_arr);
                // console.log('user_id------',data.other_user_id);
                console.log('receiver_data_arr',receiver_data_arr);
            }

            //sockets[data.other_user_id].emit('receive_message', newdataarr2);
        }


    });

     socket.on('select_payment_mode', function (data, cb) {
        var receiver_data_arr = {'data': data, 'message': 'Selected Payment Mode', 'status': 'true'};



        console.log('restaurant data -----',receiver_data_arr);

        if (sockets[data.creater_id] != undefined) {

            for (var index in sockets[data.creater_id]) {
                sockets[data.creater_id][index].emit('received_payment_mode', receiver_data_arr);
                // console.log('user_id------',data.other_user_id);
                console.log('receiver_data_arr',receiver_data_arr);
            }

            //sockets[data.other_user_id].emit('receive_message', newdataarr2);
        }


    });


    socket.on('assign_driver', function (data) {
        getDriver(data);
        var handle  = setInterval(function () {
            var sql = "SELECT * from orders where id =" + data.order_id + " and status='Accept' and driver_id  is null";
            console.log(sql);
            con.query(sql, function (err, result) {

                    if (err)
                    throw err;
                    if (result.length > 0 && handle) {
                        getDriver(data);
                    }else{
                        console.log("lengthththth" + result.length);
                        clearInterval(handle);
                        handle = false;
                    }
            });
        },20000);

    });


    function getDriver(data){
        console.log('requested data ', data);
        var sql = "SELECT * from near_by_driver_temps where order_id =" + data.order_id + " limit 1";
        con.query(sql, function (err, result) {
            if (err)
                throw err;
            console.log("already exist length==", result.length);
            console.log("order Array==", result[0]);
            if (result.length > 0) {

                var sql1 = "SELECT orders.*,u1.email as user_email,u1.user_name as user_name,u1.mobile_number as user_mobile_no,u1.lat as user_lat,u1.lon as user_lon,u1.profile_picture as user_profile_picture,u3.location as user_profile_address,u2.email as rest_email,u2.user_name as rest_name,u2.mobile_number as rest_mobile_no,u2.lat as rest_lat,u2.lon as rest_lon,u2.profile_picture as rest_profile_picture,u2.address as rest_address FROM orders ";

                sql1 += "LEFT JOIN users  AS u1 on u1.id=orders.user_id ";
                sql1 += "LEFT JOIN restaurants on restaurants.id=orders.restaurant_id ";
                sql1 += "LEFT JOIN users  AS u2 on u2.id=restaurants.user_id ";
                sql1 += "LEFT JOIN user_addresses  AS u3 on u3.user_id=orders.user_id ";
                sql1 += "WHERE orders.id="+result[0].order_id+" ";
                con.query(sql1, function (err, res) {
                    if (err)
                        throw err;
                    // console.log("order length==", res.length);
                    if (res.length > 0) {
                        // console.log("Get Booking successfully.",res[0]);
                        var message = {"status": true,
                                        "code": 200,
                                        "responseData": res[0],
                                        "user_distance": result[0].user_distance,
                                        "restaurant_distance": result[0].restaurant_distance,
                                        "msg": "Get Order Response"
                                    };

                        if (sockets[result[0].driver_id] != undefined) {
                            console.log("driver user id",result[0].driver_id);
                            sockets[result[0].driver_id][0].emit('driver_received_order', message);

                             con.query("DELETE FROM near_by_driver_temps WHERE driver_id = " + result[0].driver_id + " AND order_id = " + result[0].order_id, function (delete_err) {
                                if (err) {
                                    console.log("Temp Booking Not Delete.",delete_err);
                                }else{
                                    console.log("Temp Booking Delete Success.");
                                }
                            });

                        }

                    } else {
                        var message = {"status": false, "code": 400, "responseData": [], "requestData": data, "msg": "No Data Found"};
                        console.log("Else Order =>", message);
                    }
                });

            } else {
                var message = {"status": false, "code": 400, "responseData": [], "requestData": data, "msg": "Drivers not found."};

                 var sql_2 = "update orders set status = 'Decline' where id =" + data.order_id ;
                 con.query(sql_2,function(err,qry){
                    if(err)
                        console.log(err);

                        var sql_2 = "update orders set status = 'Decline' where id =" + data.order_id ;
                        con.query(sql_2,function(err,result1){
                            if(err)
                                console.log(err);
                                var sql = "SELECT restaurants.user_id as rest_id FROM orders ";
                                    sql += "LEFT JOIN restaurants on restaurants.id=orders.restaurant_id ";
                                    sql += "WHERE orders.id="+data.order_id+" ";

                                con.query(sql,function(err,result) {
                                    if(err)
                                    throw err;
                                        if(result.length > 0) {
                                            var message = {"status": true,
                                            "code": 200,
                                            "responseData": data.order_id,
                                            "msg": "Drivers not found"
                                        };
                                        if(sockets[result[0].rest_id] != undefined) {
                                           sockets[result[0].rest_id][0].emit('declined_order', message);
                                        }
                                    }
                                });
                        });
                });
            }
        });
    }
    socket.on('get_driver_location',function(data) {
        getDriverLocation(data);
    });

    function getDriverLocation(data) {
        console.log('requested data ', data);
        console.log("select * from users where id="+data.driver_id+"");

        var sql = "SELECT lat,lon from users where id =" + data.driver_id + " limit 1";
        con.query(sql, function (err, result) {
            if (err)
                throw err;
            console.log("already exist length==", result.length);
            console.log("order Array==", result[0]);
            if (result.length > 0) {
        console.log("result >> "+result[0].length);

                var message = {"status" : true,
                                "code" : 200,
                                "responseData" : {lat:result[0].lat,lon:result[0].lon},
                                "message" : "Driver location details"
                            };
                if(sockets[data.user_id] != undefined) {
                    sockets[data.user_id][0].emit('driver_location_data',message);
                    console.log("location get successfully ",message)

                }
            }
        })
    }

     socket.on('location_update', function (data, cb) {
        //var receiver_data_arr = {'data': data, 'message': 'Order Status', 'status': 'true'};

        console.log('location_update data -----',data);
        if (sockets[data.user_id] != undefined) {
            var update_location = "update users set lat = '" + data.latitude + "', lon = '" + data.longitude + "' where id ='" + data.user_id + "'" ;
            con.query(update_location, function (err, row) {
                if (err) {
                    console.log('update_location ', err);
                }
                else {
                    var result = [];
                    result['lat'] = data.latitude;
                    result['lon'] = data.longitude;
                    var message = {"status" : true,
                                "code" : 200,
                                "responseData" :  {lat:data.latitude,lon:data.longitude},
                                "message" : "Driver location details"
                            };
                    sockets[data.user_id][0].emit('driver_location',message);
                    console.log("location updated successfully ",message)
                }


            });

        }


    });


    socket.on('disconnect', function (eee_dis) {
        console.log("disconnect = ", user_id_temp);
        // con.query("update users set is_online='N' where id ='" + user_id_temp + "'");
        console.log('dis error', eee_dis);

        var sqlUpdate = "UPDATE users SET online='0' where id='" + user_id_temp + "'";

        con.query(sqlUpdate, function(err, result) {
        if (err)
            return console.log(err);
        });
        for (var data in sockets[socket.handshake.query.user_id]) {
          if (socket.id == sockets[socket.handshake.query.user_id][data].id) {
            console.log('sockethandshakequeryuser_id ', socket.handshake.query.user_id);
            sockets[socket.handshake.query.user_id].splice(data, 1);
          }
        }
    });

});

/*server connection*/
http.listen(port, function() {
    console.log('listening on *:'+port);
});
