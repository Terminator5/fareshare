<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\AssignDriver;
use App\Models\Order;
use App\Models\OrderDelivery;

class FindAssignDriver
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(AssignDriver $event)
    {
        $orderDelivery = OrderDelivery::where('order_id', $event->order_id)->where('status', 'Processing')->first();
        if($orderDelivery)   {
            $orderDelivery->driver_id = 1;
            $orderDelivery->created_at = date('Y-m-d H:i');
            $orderDelivery->save();
        }
    }
}
