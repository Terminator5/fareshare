<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['parent_id', 'title', 'status'];

    public static function getCategoryModel($limit, $offset, $search, $orderby,$order)
    {
        $orderby  = $orderby ? $orderby : 'id';
        $order    = $order ? $order : 'desc';

        $q        = Category::select('categories.*', 'PC.title as parent_cat_title');
        $q->leftJoin('categories as PC', 'PC.id', '=', 'categories.parent_id');
        if($search && !empty($search)){
            $q->where(function($query) use ($search) {
                $query->orWhere('categories.title', 'LIKE', $search.'%');
            });
        }

        $response   =   $q->orderBy($orderby, $order)
                            ->offset($offset)
                            ->limit($limit)
                            ->get();

        $response   =   json_decode(json_encode($response));
        return $response;
    }

    public function parent() {
    	return $this->hasOne(Category::class, 'parent_id')->select('id', 'title');
    }

    

}
