<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserBank extends Model
{
    protected $fillable = [
        'user_id', 'bank_name','branch', 'account_no', 'ifsc_code', 'is_default', 'status'
    ];

    public $timestamps = false;
}
