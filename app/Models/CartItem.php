<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    protected $fillable = ['cart_id', 'item_id', 'coupon_code_id', 'qty', 'price', 'sub_total'];
}
