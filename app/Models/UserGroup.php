<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class UserGroup extends Model {
    protected $table = 'user_groups';

    protected $fillable = ['group_name','creator_id','status'];

    public function members() {
        return $this->hasMany(GroupMember::class,'group_id');
    }

    public function creator() {
        return $this->belongsTo(User::class,'creator_id');
    }

    public function creatorGroupMember()
    {
        return $this->belongsTo(GroupMember::class,'creator_id','user_id');
    }
    public function polls()
    {
        return $this->hasMany(UserGroupPoll::class,'group_id');
    }

    public static function getUserGroupModel($limit, $offset, $search, $orderby,$order)
    {
        $orderby  = $orderby ? $orderby : 'user_groups.id';
        $order    = $order ? $order : 'desc';

        $q        = UserGroup::select('user_groups.id','user_groups.group_name','user_groups.created_at', 'u.user_name','u.email','u.mobile_number');
        $q->leftJoin('users as u', 'u.id', '=', 'user_groups.creator_id');
        if($search && !empty($search)){
            $q->where(function($query) use ($search) {
                $query->orWhere('user_groups.group_name', 'LIKE', $search.'%');
                $query->orWhere('u.user_name', 'LIKE', $search.'%');
                $query->orWhere('u.email', 'LIKE', $search.'%');
                $query->orWhere('u.mobile_number', 'LIKE', $search.'%');
            });
        }

        $response   =   $q->orderBy($orderby, $order)
                            ->offset($offset)
                            ->limit($limit)
                            ->get();

        $response   =   json_decode(json_encode($response));
        return $response;
    }
}
