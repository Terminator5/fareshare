<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $fillable = ['cart_id', 'item_id', 'coupon_code_id', 'qty', 'price', 'sub_total'];

    public $timestamps = false;


    public function item()
    {
        return $this->belongsTo(Item::class,'item_id');
    }

}
