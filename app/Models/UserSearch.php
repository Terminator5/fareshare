<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSearch extends Model
{
    public $timestamps = true;

    protected $fillable = [
        'user_id', 'search_text'
    ];
}
