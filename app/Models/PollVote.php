<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class PollVote extends Model
{
    public function voted_user_info() {
        
        return $this->belongsTo('App\User','user_id');
    }

    public function voted_restaurant_info() {
        
        return $this->belongsTo('App\Models\Restaurant','restaurant_id');
    }

    public function poll_info() {
        
        return $this->belongsTo('App\Models\UserGroupPoll','poll_id');
    }

}
