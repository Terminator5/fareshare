<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Driver extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'driver_details';

    public function getDrivingLicenceExpiryDateAttribute($value)
    {
        return date("Y/m/d", strtotime($value)); 
    }
    
    public function getVehicleRegistrationExpiryDateAttribute($value)
    {
        return date("Y/m/d", strtotime($value)); 
    }
    
    public function getVehicleInsuranceExpiryDateAttribute($value)
    {
        return date("Y/m/d", strtotime($value)); 
    }
    
    public function getVehicleYearAttribute($value)
    {
        return date("Y", strtotime($value)); 
    }

    public static function getDriverModel($limit, $offset, $search, $orderby,$order)
    {
        $orderby  = $orderby ? $orderby : 'id';
        $order    = $order ? $order : 'desc';

        $q        = User::where('role','driver')->orderBy('created_at', 'desc');
        
        if($search && !empty($search)){
            $q->where(function($query) use ($search) {
                $query->orWhere('email', 'LIKE', $search.'%');
                $query->orWhere('user_name', 'LIKE', $search.'%');
                $query->orWhere('mobile_number', 'LIKE', $search.'%');
                $query->orWhere('city', 'LIKE', $search.'%');
                $query->orWhere('state', 'LIKE', $search.'%');
                $query->orWhere('address', 'LIKE', $search.'%');
            });
        }

        $response   =   $q->orderBy($orderby, $order)
                            ->offset($offset)
                            ->limit($limit)
                            ->get();

        $response   =   json_decode(json_encode($response));
        return $response;
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    
}
