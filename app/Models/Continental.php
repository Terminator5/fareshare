<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Continental extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'continentals';

    public function getNameAttribute($value)
    {
        return strtoupper($value); 
    }
    
}
