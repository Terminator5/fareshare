<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class GroupMember extends Model
{
    protected $table = 'group_members';
    protected $fillable = ['user_id','group_id','status','payment_method','payment_method_status'];
    public $timestamps = false;

    public function group()
    {
        return $this->belongsTo(UserGroup::class)->with('creator');
    }
    public function user() {
        return $this->belongsTo(User::class,'user_id');
    }

}
