<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PollRestaurants extends Model
{
    protected $table = 'poll_restaurants';
    protected $fillable = ['restaurant_id'];
    public $timestamps = false;

    public function poll()
    {
        return $this->belongsTo(UserGroupPoll::class,'poll_id');
    }

    public function restaurantDetail()
    {
        return $this->belongsTo(Restaurant::class,'restaurant_id');
    }
}
