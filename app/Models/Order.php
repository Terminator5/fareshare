<?php

namespace App\Models;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use App\User;
use DB;

class Order extends Model
{
    // protected $appends = ['delivery_fee'];
    // public function getDeliveryFeeAttribute() {
    //     return WalletPercentage::where(['role_id'=>4])->first()->percentage ?? 0;
    // }
    public function distanceTime($restaurant_id, $lat, $lon)
    {

        $key = env('GOOGLE_MAP_KEY');
        $restaurant = Restaurant::select('U.lat','U.lon')->join('users as U', 'U.id','=','restaurants.user_id')->find($restaurant_id);

        if($restaurant->lat && $restaurant->lon && $lat && $lon) {
            $url = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins='.$lat.','.$lon.'&destinations='.$restaurant->lat.'%2C'.$restaurant->lon.'&mode=driving&key='.$key;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $response = curl_exec($ch);
            curl_close($ch);
            $response_a = json_decode($response, true);
            $status = $response_a['rows'][0]['elements'][0]['status'];
            if ( $status == 'ZERO_RESULTS' || $status == 'NOT_FOUND' )
            {
                return array(null, null);
            }
            $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
            $time = $response_a['rows'][0]['elements'][0]['duration']['text'];
            if($dist && $time) {
                // print_r(array((float) $dist, $time. " away"));exit;
                return array((float) $dist, $time. " away");
            } else {
                return array(null, null);
            }

        }
        return array(null, null);
    }

    public static function getItemModel($limit, $offset, $search, $orderby,$order)
    {
        $orderby  = $orderby ? $orderby : 'orders.id';
        $order    = $order ? $order : 'desc';

        $q        = Order::select('orders.*', 'U.user_name', 'R.restaurant_name');
        $q->leftJoin('users as U', 'U.id', '=', 'orders.user_id');
        $q->leftJoin('restaurants as R', 'R.id', '=', 'orders.restaurant_id');
        if($search && !empty($search)){
            $q->where(function($query) use ($search) {
                $query->orWhere('orders.order_type', 'LIKE', $search.'%');
                $query->orWhere('orders.total_amount', 'LIKE', $search.'%');
                $query->orWhere('orders.grand_total', 'LIKE', $search.'%');
                $query->orWhere('orders.payment_type', 'LIKE', $search.'%');
                $query->orWhere('orders.status', 'LIKE', $search.'%');
                $query->orWhere('orders.payment_status', 'LIKE', $search.'%');
                $query->orWhere('R.restaurant_name', 'LIKE', $search.'%');
                $query->orWhere('U.user_name', 'LIKE', $search.'%');
            });
        }

        $response   =   $q->orderBy($orderby, $order)
                            ->offset($offset)
                            ->limit($limit)
                            ->get();

        $response   =   json_decode(json_encode($response));
        return $response;
    }

    public function items() {
    	return $this->hasMany(OrderItem::class, 'order_id')->with('item');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function driver() {
        return $this->belongsTo(Driver::class, 'driver_id')->with('user');
    }

    public function shipping_address() {
        return $this->hasOne(OrderShippingAddress::class, 'order_id');
    }

    public function restaurant(){
    	return $this->belongsTo(Restaurant::class, 'restaurant_id')->with('owner');
    }

    public function customer(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function findNearDriver($order_id) {
        // Order detail
        $order = Order::find($order_id);

        $owner = $order->restaurant->owner;
        // Get Restaurant patner drivers
        $drivers = DriverPartner::where('restaurant_id' , $order->restaurant_id)
        ->join('driver_details as D', 'D.id', '=', 'driver_partners.driver_id')
        ->pluck('D.user_id');

        // Get top rating Driver
        $setting = Setting::where('slug','driver_delivery_radius')->first();
        // echo "<pre>";
        // print_r($setting);exit;
        if($setting) {
            $driver_delivery_radius = $setting->value;
        }
        else {
            $driver_delivery_radius = 50;
        }
        $startDis = env('START_DIS',$driver_delivery_radius);
        $endDis = env('END_DIS','100');
        $q = User::select('id', DB::raw("( 6371 * acos( cos( radians(".$owner->lat.") ) * cos( radians( lat ) )
                    * cos( radians( lon ) - radians(".$owner->lon.") ) + sin( radians(".$owner->lat.") ) * sin(radians(lat)) ) ) AS distance"));
        $q->where('role', 'driver');
        $q->where('online', '1');
        if(count($drivers)>0)	$q->orWhereIn('id', $drivers);
        $q->havingRaw(' distance <= '.$startDis)
        ->orderBy('distance', 'asc')
        ->limit(5);

        $driver = $q->get();
            // echo "<pre>";
            // print_r($driver->toArray());die;
        // return $driver = DB::select($q);
        return $driver;
    }
    public function assignAllDriverRequest($driverArray,$order){


        foreach($driverArray as $driver) {
            $nearByDriver['order_id'] = $order->id;
            $nearByDriver['driver_id'] = $driver->id;
            $nearByDriver['restaurant_distance'] = 0;
            $nearByDriver['user_distance'] = 0;

            $driver = $this->getUserLatLon($driver->id);

            $restaurant = Restaurant::select('U.lat','U.lon')->join('users as U', 'U.id','=','restaurants.user_id')->find($order->restaurant_id);



            if($driver && $restaurant){
                // Calculate distance between restaurant to driver
                $resToDriverDistance = (new \App\Http\Controllers\Controller)->getDistance($restaurant->lat, $restaurant->lon, $driver->lat, $driver->lon);
                $nearByDriver['restaurant_distance'] = $resToDriverDistance ?? 0;
            }
            if($restaurant){

                // Calculate distance between resturant to user
                $resToUserDistance = (new \App\Http\Controllers\Controller)->getDistance($restaurant->lat, $restaurant->lon, $order->shipping_address->lat, $order->shipping_address->lon);
                $nearByDriver['user_distance'] = $resToUserDistance ?? 0;
            }

            $nearByDriverArray[] = $nearByDriver;

        }

        NearByDriverTemp::insert($nearByDriverArray);

    }
    public function getUserLatLon($user_id) {
        return  User::select('lat','lon')->where('id',$user_id)->first();
    }
    public function addOrderShippingAddresses($order_id,$user_id,$userAddress){
        if($userAddress){
            $otherShippingAddress = new OrderShippingAddress();
            $otherShippingAddress->order_id = $order_id;
            $otherShippingAddress->location = $userAddress->location;
            $otherShippingAddress->house_no = $userAddress->house_number;
            $otherShippingAddress->landmark = $userAddress->landmark;
            $otherShippingAddress->lat = $userAddress->lat;
            $otherShippingAddress->lon = $userAddress->lon;
            $otherShippingAddress->save();
        }


    }
    public function DriverRating()
    {
        return $this->hasOne(DriverRating::class);
    }
    public function DriverInfo()
    {
        return $this->belongsTo(User::class,'driver_id');
    }



    //////////////////////////////////////////////////////////////////////////////////////////
}
