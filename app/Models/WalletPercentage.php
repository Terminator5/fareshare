<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WalletPercentage extends Model
{
    //
    public function role()
    {
    	return $this->belongsTo(Role::class);
    }
    public static function getWalletPercentageModel($limit, $offset, $search, $orderby,$order)
    {
        $orderby  = $orderby ? $orderby : 'id';
        $order    = $order ? $order : 'desc';

        $q        = WalletPercentage::select('*');
        if($search && !empty($search)){
            $q->where(function($query) use ($search) {
                $query->orWhere('percentage', 'LIKE', $search.'%');
            });
        }

        $response   =   $q->orderBy($orderby, $order)
                            ->offset($offset)
                            ->limit($limit)
                            ->with('role')
                            ->get();

        $response   =   json_decode(json_encode($response));
        return $response;
    }
}
