<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $fillable = ['user_id', 'restaurant_id','group_id', 'discount', 'tax', 'total_amount', 'grand_total'];
    protected $appends = ['delivery_fee','order_tax'];

    public function cartItems() {
        return $this->hasMany(CartItem::class, 'cart_id');
    }

    public function restaurant() {
        return $this->belongsTo(Restaurant::class);
    }
    public function getDeliveryFeeAttribute() {
        return WalletPercentage::where(['role_id'=>4])->first()->percentage ?? 0;
    }
    public function getOrderTaxAttribute() {
        return Setting::where(['slug'=>'tax'])->first()->value ?? 0;
    }
    public function group() {
        return $this->belongsTo(UserGroup::class,'group_id');
    }
}
