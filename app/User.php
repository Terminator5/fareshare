<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Driver;
use App\Models\Restaurant;
use App\Models\RestaurantMedia;
use App\Models\RestaurantContinental;
use App\Models\Role;
use App\Models\UserAddress;
use App\Models\Cart;
use App\Models\GroupMember;
use App\Models\Order;
use App\Models\UserGroup;
use App\Models\Wallet;
use App\Models\WalletTransaction;
use App\Models\DriverRating;
use App\Models\RestaurantFavourite;
use App\Models\Notification;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Laravel\Cashier\Billable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable,Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'profile_picture', 'user_name', 'country_code', 'mobile_number', 'status', 'social_type', 'social_id', 'city', 'state', 'signup_role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = ['profile_picture_path', 'restaurant_id', 'driver_id', 'current_role','user_address', 'cart_id','is_active_order'];

    public function getCartIdAttribute($value) {
        $cart = Cart::where('user_id', $this->id)->first();
        if($cart) {
            return $cart->id;
        } else {
            return null;
        }
    }
    public function getIsActiveOrderAttribute() {
        return Order::where(['user_id'=>$this->id,'active_order'=>1])->count() ?? 0;
    }

    public function getUserAddressAttribute($value) {
        $address = UserAddress::where('user_id', $this->id)->where('is_default',1)->first();
        if($address) {
            return $address;
        } else {
            return null;
        }
    }

    public function getProfilePicturePathAttribute($value)
    {
        if(isset($this->profile_picture) && !empty($this->profile_picture)){
            return url($this->profile_picture);
        }else{
            return null;
        }

    }

    public function getCurrentRoleAttribute($value)
    {
        $role = Role::where('role', 'LIKE', strtolower($this->role))->first();
        if($role){
            return $role->role;
        }else{
            return null;
        }

    }

    public function getRestaurantIdAttribute($value)
    {
        $restaurant = Restaurant::where('user_id', $this->id)->first();
        if($restaurant){
            return $restaurant->id;
        }else{
            return null;
        }

    }

    public function getDriverIdAttribute($value)
    {
        $driver = Driver::where('user_id', $this->id)->first();
        if($driver){
            return $driver->id;
        }else{
            return null;
        }

    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public static function getUserModel($limit, $offset, $search, $orderby,$order)
    {
        $orderby  = $orderby ? $orderby : 'id';
        $order    = $order ? $order : 'desc';

        // $q        = User::where('is_admin', '!=', '1');
        $q        = User::where('is_admin', '!=', '1')->where('role', '=', 'user');

        if($search && !empty($search)){
            $q->where(function($query) use ($search) {
                $query->orWhere('user_name', 'LIKE', $search.'%');
                $query->orWhere('email', 'LIKE', $search.'%');
                $query->orWhere('mobile_number', 'LIKE', $search.'%');
            });
        }

        $response   =   $q->orderBy($orderby, $order)
                            ->offset($offset)
                            ->limit($limit)
                            ->get();

        $response   =   json_decode(json_encode($response));
        return $response;
    }


    public function driver()
    {
        return $this->hasOne(Driver::class, 'user_id');
    }

    public function restaurant()
    {
        return $this->hasOne(Restaurant::class, 'user_id');
    }

    public function restaurantMedia()
    {
        return $this->hasMany(RestaurantMedia::class, 'user_id');
    }

    public function groups() {
        return $this->hasMany(GroupMember::class,'user_id');
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
    // public function groupMember()
    // {
    //     return $this->belongsTo(GroupMember::class,'user_id');
    // }

    public function wallet()
    {
        return $this->hasOne(Wallet::class);
    }
    public function walletTransactions()
    {
        return $this->hasMany(WalletTransaction::class);
    }
    public function DriverRating()
    {
        return $this->hasOne(DriverRating::class);
    }
    public function RestaurantFavourite()
    {
        return $this->hasOne(RestaurantFavourite::class);
    }
    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }
    public function DwollaCustomerFunding() {
        return $this->hasMany(DwollaCustomerFunding::class);
    }







    /////////////////////////////////////////////////////////////////
}
