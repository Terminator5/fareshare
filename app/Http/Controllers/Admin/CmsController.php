<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Cms;
use Illuminate\Support\Facades\Validator;

class CmsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->sortable_columns = [
            0 => 'title',
            1 => 'updated_at',
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax())
        {
            $totalCms       = Cms::count();
            $limit          = $request->input('length');
            $start          = $request->input('start');
            $search         = $request['search']['value'];
            $orderby        = $request['order']['0']['column'];
            $order          = $orderby != "" ? $request['order']['0']['dir'] : "";
            $draw           = $request['draw'];

            $response       = Cms::getCmsModel($limit , $start , $search, $this->sortable_columns[$orderby], $order);

            if(!$response){
                $page       = [];
                $paging     = [];
            }
            else{
                $page       = $response;
                $paging     = $response;
            }

            $data = array();

            foreach ($page as $cms) {
            	$u['slug']         = ($cms->slug);
            	$u['id']         = ($cms->id);
                $u['title']         = ucwords($cms->title);
                $u['created_at']    = date('d/m/Y', strtotime($cms->created_at));

                $cms_status         = view('admin.cms.status', ['id' => $cms->id, 'status' => $cms->status]);
                $u['status']        = $cms_status->render();

                $actions            = view('admin.cms.actions', ['id' => $cms->id, 'status' => $cms->status ]);
                $u['actions']       = $actions->render();

                $data[] = $u;

                unset($u);
            }

            $return = [
                "draw"              =>  intval($draw),
                "recordsFiltered"   =>  intval( $totalCms),
                "recordsTotal"      =>  intval( $totalCms),
                "data"              =>  $data
            ];
            return $return;
        }

        $page_title = "CMS";
        return view('admin.cms.index')
            ->with('page_title', $page_title);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // dd('No action found');
        $page_title  =  "Create Cms";
        return view('admin.cms.create',compact('page_title'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd('No action found');


        $input  =  $request->all();

        $validator = Validator::make($request->all(), [
            'slug'  => 'required|string|min:3|max:200|unique:cms',
            'title'  =>   'required|string|min:3|max:200',
            'content'         => 'required'
        ]);

        if($validator->fails()) {
            $errors = $validator->errors();

            $request->session()->flash('alert-danger', 'Errors! Please correct the following errors and submit again.');
            return back()->withErrors($errors)->withInput();
        }
        else{

            $cms                     = new Cms;
            $cms->title              = $request->title;
            $cms->slug              = $request->slug;
            $cms->content            = $request->content;
            //$cms->meta_title         = $request->meta_title;
            //$cms->meta_description   = $request->meta_description;
            //$cms->meta_tags          = $request->meta_tags;
           $cms->status             = $request->status;
            $cms->save();

            $request->session()->flash('alert-success', 'Created successfully.');
            return redirect('admin/cms');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id    = ($id);
        $cms   = Cms::where('id', $id)->first();

        $data  = [
            "page_title"     =>  "Edit Cms",
            "cms"          =>  $cms
        ];
        return view('admin.cms.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id     =  $id;
        $input  =  $request->all();

        $validator = Validator::make($request->all(), [
            'slug'  => 'required|string|min:3|max:200|unique:cms,slug,'.$id,
            'title'  =>   'required|string|min:3|max:200',
            'content'         => 'required'
        ]);

        if($validator->fails()) {
            $errors = $validator->errors();

            $request->session()->flash('alert-danger', 'Errors! Please correct the following errors and submit again.');
            return back()->withErrors($errors)->withInput();
        }
        else{

            $cms                     = Cms::findOrFail( $id );
            $cms->title              = $request->title;
            $cms->slug              = $request->slug;
            $cms->content            = $request->content;
            //$cms->meta_title         = $request->meta_title;
            //$cms->meta_description   = $request->meta_description;
            //$cms->meta_tags          = $request->meta_tags;
           $cms->status             = $request->status;
            $cms->save();

            $request->session()->flash('alert-success', 'Updated successfully.');
            return redirect('admin/cms');
        }
    }

    /**
     * Active
     *
     * @param  int  $id
     */
    public function active($id)
    {
        $cms = Cms::find($id);
        $cms->status = '1';
        $cms->save();

        return response()->json(array('success' => true));
    }

    /**
     * In-Active
     *
     * @param  int  $id
     */
    public function inActive($id)
    {
        $cms = Cms::find($id);
        $cms->status = '0';
        $cms->save();

        return response()->json(array('success' => true));
    }

    public function detail($id) {

        $page_title = 'Cms Detail';
        $cms = Cms::findOrFail($id);
        return view('admin.cms.detail', compact('page_title', 'cms'));
    }


}
