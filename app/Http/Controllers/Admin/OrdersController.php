<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;

class OrdersController extends Controller
{
    public function __construct()
    {
        $this->sortable_columns = [
            0 => 'id',
            1 => 'user_id',
            2 => 'restaurant_id',
            3 => 'order_type',
            4 => 'total_amount',
            5 => 'grand_total',
            6 => 'payment_type',
            7 => 'status',
            8 => 'payment_status',
            9 => 'created_at'
        ];
    }

	public function index(Request $request) {
		if($request->ajax())
        {
            $totalCms       = Order::count();
            $limit          = $request->input('length');
            $start          = $request->input('start');
            $search         = $request['search']['value'];
            $orderby        = $request['order']['0']['column'];
            if($this->sortable_columns[$orderby] == 'id') {
                $order          = $orderby != "" ? 'desc' : "";
            }
            else {
                $order          = $orderby != "" ? $request['order']['0']['dir'] : "";
                // echo $this->sortable_columns[$orderby];exit;

            }
            $draw           = $request['draw'];
            $response       = Order::getItemModel($limit , $start , $search, $this->sortable_columns[$orderby], $order);

            if(!$response){
                $page       = [];
                $paging     = [];
            }
            else{
                $page       = $response;
                $paging     = $response;
            }

            $data = array();

            foreach ($page as $item) {
            	$u['id']         = ($item->id);
                $u['user_id']         = ucwords($item->user_name);
                $u['restaurant_id']         = ucwords($item->restaurant_name);
                $u['order_type']         = ($item->order_type);
                $u['total_amount']         = ($item->total_amount);
                $u['grand_total']         = ($item->grand_total);
                $u['payment_type']         = ucwords($item->payment_type);
                $u['status']         = ucwords($item->status);
                $u['payment_status']         = ucwords($item->payment_status);
                $u['created_at']    = date('d/m/Y', strtotime($item->created_at));

                //$status         = view('admin.categories.status', ['id' => $category->id, 'status' => $category->status]);
                //$u['status']        = $status->render();

                //$actions            = view('admin.categories.actions', ['id' => $category->id, 'status' => $category->status ]);
                $u['actions']       = '<a data-toggle="tooltip" class="btn btn-primary btn-xs" title="View" href="'.url('admin/orders/'.$item->id).'">
                                        <i class="fa fas fa-eye" aria-hidden="true"></i></a>';

                $data[] = $u;

                unset($u);
            }

            $return = [
                "draw"              =>  intval($draw),
                "recordsFiltered"   =>  intval( $totalCms),
                "recordsTotal"      =>  intval( $totalCms),
                "data"              =>  $data
            ];
            return $return;
        }

		$page_title = 'Order List';
        return view('admin.orders.index', compact('page_title'));
    }
    public function show(Request $request,Order $order)
    {
        $page_title = 'Order Detail';
        return view('admin.orders.show',compact('page_title','order'));
    }
}
