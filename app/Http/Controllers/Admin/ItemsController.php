<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use App\Models\Item;
use Form;

class ItemsController extends Controller
{
    public function __construct()
    {
        $this->sortable_columns = [
            0 => 'id',
            1 => 'item_title',
            2 => 'category_id',
            3 => 'item_price',
            4 => 'coupon_code_id',
            5 => 'restaurant_id'
        ];
    }

	public function index(Request $request) {
		if($request->ajax())
        {
            $totalCms       = Item::count();
            $limit          = $request->input('length');
            $start          = $request->input('start');
            $search         = $request['search']['value'];
            $orderby        = $request['order']['0']['column'];
            $order          = $orderby != "" ? $request['order']['0']['dir'] : "";
            if($this->sortable_columns[$orderby] == 'id') {
                $order          = $orderby != "" ? 'desc' : "";
            }
            else {
                $order          = $orderby != "" ? $request['order']['0']['dir'] : "";
                // echo $this->sortable_columns[$orderby];exit;

            }
            $draw           = $request['draw'];

            $response       = Item::getItemModel($limit , $start , $search, $this->sortable_columns[$orderby], $order);

            if(!$response){
                $page       = [];
                $paging     = [];
            }
            else{
                $page       = $response;
                $paging     = $response;
            }

            $data = array();

            foreach ($page as $item) {
            	$u['id']         = ($item->id);
                $u['item_title']                = ucwords($item->item_title);
                $u['category_id']               = ucwords($item->cat_title);
                $u['item_price']                = ($item->item_price);
                $u['coupon_code']               = ($item->coupon_code);
                $u['coupon_code_percentage']    = ($item->coupon_code_percentage).'%';
                $u['restaurant_id']             = ucwords($item->restaurant->restaurant_name ?? "") ?? "";
                $u['created_at']                = date('d/m/Y', strtotime($item->created_at));

                //$status         = view('admin.categories.status', ['id' => $category->id, 'status' => $category->status]);
                //$u['status']        = $status->render();

                // $actions            = view('admin.items.actions', ['id' => $item->id, 'status' => $item->status ]);
                $u['actions']       = '<a data-toggle="tooltip" class="btn btn-primary btn-xs" title="View" href="'.url('admin/items/'.$item->id).'">
                <i class="fa fas fa-eye" aria-hidden="true"></i></a>';

                $data[] = $u;

                unset($u);
            }

            $return = [
                "draw"              =>  intval($draw),
                "recordsFiltered"   =>  intval( $totalCms),
                "recordsTotal"      =>  intval( $totalCms),
                "data"              =>  $data
            ];
            return $return;
        }

		$page_title = 'Item List';
        return view('admin.items.index', compact('page_title'));
	}
    public function show($id)
    {
        $item = Item::find($id);
        $page_title = 'Item Detail';
        return view('admin.items.show',compact('page_title','item'));
    }

}
