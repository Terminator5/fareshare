<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use App\User;

class UsersController extends Controller
{

	public function __construct()
    {
        $this->sortable_columns = [
            0 => 'id',
            1 => 'user_name',
            2 => 'email',
            3 => 'mobile_number',
            4 => 'address',
            5 => 'role',
        ];
    }

	public function index(Request $request) {
		if($request->ajax())
        {
            $totalCms       = User::where('role', '=', 'user')->count();
            $limit          = $request->input('length');
            $start          = $request->input('start');
            $search         = $request['search']['value'];
            $orderby        = $request['order']['0']['column'];
            $order          = $orderby != "" ? $request['order']['0']['dir'] : "";
            $draw           = $request['draw'];
          
            $response       = User::getUserModel($limit , $start , $search, $this->sortable_columns[$orderby], $order);

            if(!$response){
                $page       = [];
                $paging     = [];
            }
            else{
                $page       = $response;
                $paging     = $response;
            }

            $data = array();

            foreach ($page as $user) { 
            	$u['id']         = ($user->id);
                $u['user_name']         = ucwords($user->user_name);
                $u['email']         = ($user->email);
                $u['mobile_number']   = '+'.$user->country_code .'-' . $user->mobile_number;
                $u['address']         = ($user->address);
                $u['role']         = (ucfirst($user->role));
                $u['created_at']    = date('d/m/Y', strtotime($user->created_at));

                $status         = view('admin.users.status', ['id' => $user->id, 'status' => $user->status]);
                $u['status']        = $status->render();

                $actions            = view('admin.users.actions', ['id' => $user->id, 'status' => $user->status ]);
                $u['actions']       = $actions->render(); 

                $data[] = $u;

                unset($u);
            }

            $return = [
                "draw"              =>  intval($draw),
                "recordsFiltered"   =>  intval( $totalCms),
                "recordsTotal"      =>  intval( $totalCms),
                "data"              =>  $data
            ];
            return $return;
        }

		$page_title = 'User List';
        return view('admin.users.index', compact('page_title'));
	}

	/**
     * Active 
     *
     * @param  int  $id
     */
    public function active($id)
    {
        $user = User::find($id);
        $user->status = '1';
        $user->save();

        return response()->json(array('success' => true));
    }

    /**
     * In-Active
     *
     * @param  int  $id
     */
    public function inActive($id)
    {
        $user = User::find($id);
        $user->status = '0';
        $user->save();

        return response()->json(array('success' => true));
    }

    public function detail($id) {
		
		$page_title = 'User Detail';
		$user = User::findOrFail($id);
        return view('admin.users.detail', compact('page_title', 'user'));
	}

}