<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use App\User;
use App\Models\Restaurant;

class VendorsController extends Controller
{

	public function __construct()
    {
        $this->sortable_columns = [
            0 => 'id',
            1 => 'email',
            2 => 'user_name',
            3 => 'mobile_number',
            4 => 'city',
            5 => 'state',
            6 => 'country',
            // 7 => 'address',
        ];
    }

	public function index(Request $request) {
		if($request->ajax())
        {
            $totalCms       = Restaurant::count();
            $limit          = $request->input('length');
            $start          = $request->input('start');
            $search         = $request['search']['value'];
            $orderby        = $request['order']['0']['column'];
            $order          = $orderby != "" ? $request['order']['0']['dir'] : "";
            $draw           = $request['draw'];

            $response       = Restaurant::getRestaurantModel($limit , $start , $search, $this->sortable_columns[$orderby], $order);

            if(!$response){
                $page       = [];
                $paging     = [];
            }
            else{
                $page       = $response;
                $paging     = $response;
            }

            $data = array();

            foreach ($page as $vendor) {
            	$u['id']         = ($vendor->id);
                $u['email']         = ucwords($vendor->email);
                $u['user_name']         = ($vendor->user_name);
                $u['mobile_number']         = '+'.$vendor->country_code .'-' . ($vendor->mobile_number);
                $u['city']         = ($vendor->city);
                $u['state']         = (ucwords($vendor->state));
                $u['country']         = ($vendor->country);
                // $u['address']         = ($vendor->address);
                $u['created_at']    = date('d/m/Y', strtotime($vendor->created_at));

                $status         = view('admin.vendors.status', ['id' => $vendor->id, 'status' => $vendor->status]);
                $u['status']        = $status->render();

                $actions            = view('admin.vendors.actions', ['id' => $vendor->id, 'status' => $vendor->status ]);
                $u['actions']       = $actions->render();

                $data[] = $u;

                unset($u);
            }

            $return = [
                "draw"              =>  intval($draw),
                "recordsFiltered"   =>  intval( $totalCms),
                "recordsTotal"      =>  intval( $totalCms),
                "data"              =>  $data
            ];
            return $return;
        }

		$page_title = 'Vendors List';
        return view('admin.vendors.index', compact('page_title'));
	}

	/**
     * Active
     *
     * @param  int  $id
     */
    public function active($id)
    {
        $restaurant = User::find($id);
        $restaurant->status = '1';
        $restaurant->save();

        return response()->json(array('success' => true));
    }

    /**
     * In-Active
     *
     * @param  int  $id
     */
    public function inActive($id)
    {
        $restaurant = User::find($id);
        $restaurant->status = '0';
        $restaurant->save();

        return response()->json(array('success' => true));
    }

    public function detail($id) {

		$page_title = 'Vendor Detail';
        $restaurant = User::where('id',$id)->with('restaurant','restaurantMedia')->first();
		// $restaurant = Restaurant::where('id',$id)->with('media')->first();
        // $restaurant = Restaurant::findOrFail($id);
        return view('admin.vendors.detail', compact('page_title', 'restaurant'));
    }
    public function vendor_agreement($id)
    {
        $user = User::find($id);
        $page_title = 'Vendor Agreement Document';


        try {
            $mpdf = new \Mpdf\Mpdf([
              'tempDir' => __DIR__ . '/../tmp', // uses the current directory's parent "tmp" subfolder
              'setAutoTopMargin' => 'stretch',
              'setAutoBottomMargin' => 'stretch'
            ]);

        $html = view('admin.vendors.vendor-agreement', compact('user','page_title'))->render();


        $mpdf->WriteHTML($html);
        $mpdf->Output();
          } catch (\Mpdf\MpdfException $e) {
              print "Creating an mPDF object failed with" . $e->getMessage();
          }
    }

}
