<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use App\Models\Category;
use Form;

class CategoriesController extends Controller
{
    public function __construct()
    {
        $this->sortable_columns = [
            0 => 'id',
            1 => 'parent_id',
            2 => 'title',
        ];
    }

	public function index(Request $request) {
		if($request->ajax())
        {
            $totalCms       = Category::count();
            $limit          = $request->input('length');
            $start          = $request->input('start');
            $search         = $request['search']['value'];
            $orderby        = $request['order']['0']['column'];
            $order          = $orderby != "" ? $request['order']['0']['dir'] : "";
            $draw           = $request['draw'];
          
            $response       = Category::getCategoryModel($limit , $start , $search, $this->sortable_columns[$orderby], $order);

            if(!$response){
                $page       = [];
                $paging     = [];
            }
            else{
                $page       = $response;
                $paging     = $response;
            }

            $data = array();

            foreach ($page as $category) { 
            	$u['id']         = ($category->id);
                $u['parent_id']         = ucwords($category->parent_cat_title);
                $u['title']         = ucwords($category->title);
                $u['created_at']    = date('d/m/Y', strtotime($category->created_at));

                $status         = view('admin.categories.status', ['id' => $category->id, 'status' => $category->status]);
                $u['status']        = $status->render();

                $actions            = view('admin.categories.actions', ['id' => $category->id, 'status' => $category->status ]);
                $u['actions']       = $actions->render(); 

                $data[] = $u;

                unset($u);
            }

            $return = [
                "draw"              =>  intval($draw),
                "recordsFiltered"   =>  intval( $totalCms),
                "recordsTotal"      =>  intval( $totalCms),
                "data"              =>  $data
            ];
            return $return;
        }

		$page_title = 'Category List';
        return view('admin.categories.index', compact('page_title'));
	}

	public function create() {
		
		$categories = Category::where('parent_id',0)->pluck('title','id');
		$page_title = 'Create Category';
        return view('admin.categories.create', compact('page_title', 'categories'));
	}

	public function store(Request $request) {
		$category = new Category;
		$validator = Validator::make($request->all(), [         
            'title'  =>   'required|string|min:3|max:200',
        ]);

        if($validator->fails()) {
            $errors = $validator->errors();

            $request->session()->flash('alert-danger', 'Errors! Please correct the following errors and submit again.');
            return back()->withErrors($errors)->withInput();
        }
		$category->parent_id = $request->parent_id ?? 0;
		$category->title = $request->title;
		$category->save();

		$request->session()->flash('alert-success', 'Category created successfully.');
		return redirect('/admin/categories');
	}

	public function edit($id) {
		
		$category = Category::find($id);
		$categories = Category::where('parent_id',0)->pluck('title','id');
		$page_title = 'Edit Category';
        return view('admin.categories.edit', compact('page_title', 'category', 'categories'));
	}

	public function update(Request $request, $id) {
		$category = Category::find($id);
		$validator = Validator::make($request->all(), [         
            'title'  =>   'required|string|min:3|max:200',
        ]);

        if($validator->fails()) {
            $errors = $validator->errors();

            $request->session()->flash('alert-danger', 'Errors! Please correct the following errors and submit again.');
            return back()->withErrors($errors)->withInput();
        }
		$category->parent_id = $request->parent_id ?? 0;
		$category->title = $request->title;
		$category->save();

		$request->session()->flash('alert-success', 'Category updated successfully.');
		return redirect('/admin/categories');
	}


	/**
     * Active 
     *
     * @param  int  $id
     */
    public function active($id)
    {
        $row = Category::find($id);
        $row->status = '1';
        $row->save();

        return response()->json(array('success' => true));
    }

    /**
     * In-Active
     *
     * @param  int  $id
     */
    public function inActive($id)
    {
        $row = Category::find($id);
        $row->status = '0';
        $row->save();

        return response()->json(array('success' => true));
    }
}
