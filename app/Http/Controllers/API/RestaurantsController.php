<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use JWTAuth;
use Hash;
use App\Models\Restaurant;
use App\Models\Category;
use App\Models\Item;

use App\Models\CartItem;
use App\Models\Continental;
use App\Models\DriverPartner;
use App\Models\Driver;
use App\Models\RestaurantFavourite;
use Illuminate\Support\Facades\DB;

class RestaurantsController extends Controller
{

	public function getRestaurant(Request $request) {
        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;
		$validator = Validator::make($request->all(), [
            'restaurant_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        }
        $restaurant_id = $request->restaurant_id;
        $restaurant = Restaurant::findOrFail($request->restaurant_id);
        if($request->group_id && $request->group_id > 0) {
            $group_id = $request->group_id;
        }
        else {
            $group_id = 0;
        }
        //$restaurant->categories = $restaurant->categories($request->restaurant_id);
        $categories = Category::where('status',1)->where('parent_id',0)->get();
        $categories->each(function($category, $key) use($restaurant_id,$userId,$group_id) {
            $category->items = Item::where('status',1)->where('restaurant_id', $restaurant_id)->where('category_id', $category->id)->get();
            foreach($category->items as $key => $item) {
                $cartItem = DB::table('carts')->join('cart_items','carts.id','=' ,'cart_items.cart_id')
                ->where(['carts.user_id' => $userId,'carts.restaurant_id' => $restaurant_id,'cart_items.item_id' => $item->id])
                ->where('carts.group_id',$group_id)
                ->first();

                if($cartItem) {
                    $category->items[$key]['qty'] = $cartItem->qty;
                }
            }

        });

        $restaurant->categories = $categories;

        $is_favourite = RestaurantFavourite::where('restaurant_id',$request->restaurant_id)->where('user_id',$userId);
        if($is_favourite->exists()){
            $favourite = $is_favourite->first();
            $restaurant->is_favourite = ($favourite->status==1)?true:false;
        }else{
            $restaurant->is_favourite = false;
        }


        $restaurant->owner;
        list($distance, $time) = $restaurant->distanceTime($request->restaurant_id, $request->lat, $request->lon);
        $restaurant->distance_time = $time;
        $restaurant->distance = $distance;
        $response['status'] = true;
        $response['message'] = "Restaurant detail.";
        $response['data'] = $restaurant;
    	return response()->json($response);
    }

	public function getRestaurantVegItems(Request $request) {
        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;
		$validator = Validator::make($request->all(), [
            'restaurant_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        }

        $restaurant_id = $request->restaurant_id;
        $restaurant = Restaurant::findOrFail($request->restaurant_id);
        //$restaurant->categories = $restaurant->categories($request->restaurant_id);
        $categories = Category::where('status',1)->where('parent_id',0)->get();
        $categories->each(function($category, $key) use($restaurant_id,$userId) {
             $category->items = Item::where('status',1)->where('restaurant_id', $restaurant_id)->where('category_id', $category->id)->where('item_type','Vegetarian')->get();
             foreach($category->items as $key => $item) {
                $cartItem = DB::table('carts')->join('cart_items','carts.id','=' ,'cart_items.cart_id')
                ->where(['carts.user_id' => $userId,'carts.restaurant_id' => $restaurant_id,'cart_items.item_id' => $item->id])
                ->first();
                if($cartItem) {
                    $category->items[$key]['qty'] = $cartItem->qty;
                }
            }
        });

        $restaurant->categories = $categories;

        $restaurant->owner;
        list($distance, $time) = $restaurant->distanceTime($request->restaurant_id, $request->lat, $request->lon);
        $restaurant->distance_time = $time;
        $restaurant->distance = $distance;
        $response['status'] = true;
        $response['message'] = "Restaurant detail.";
        $response['data'] = $restaurant;
    	return response()->json($response);
	}

    public function searchItemRestaurant(Request $request) {
        $validator = Validator::make($request->all(), [
            'keyword' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        }

        $restaurants = Restaurant::where('status', 1)->where('restaurant_name', 'Like', '%'.$request->keyword.'%')->get();

        $items = Item::where('status', 1)->whereHas('category',function($q){
            $q->where('status',1);
        })->where('item_title', 'Like', '%'.$request->keyword.'%')->get();

        $merged = $restaurants->merge($items);

        $response['status'] = true;
        $response['message'] = "Search data.";
        $response['data'] = $merged;
        return response()->json($response);

    }

    public function addDriverPartner(Request $request) {
        $restaurant_id = JWTAuth::toUser(JWTAuth::getToken())->restaurant_id;

        $validator = Validator::make($request->all(), [
            'driver_code' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        }

        $driver = Driver::where('driver_code', $request->driver_code)->first();
        if($driver) {
            if(DriverPartner::where(['restaurant_id'=> $restaurant_id, 'driver_id'=>$driver->id])->first()) {
                $response['message'] = "Driver already exist.";
            }
            else {
                $response['message'] = "Driver added successfully.";
            }
            DriverPartner::firstOrCreate(['restaurant_id'=> $restaurant_id, 'driver_id'=>$driver->id]);

        } else {
            return response()->json(['status'=>false,'message' => "Driver not found."]);
        }

        $driverPartners = DriverPartner::where('restaurant_id', $restaurant_id)->get();
        $response['status'] = true;
        $response['data'] = $driverPartners;
        return response()->json($response);

    }



    public function driverPartnerList(Request $request) {
        $restaurant_id = JWTAuth::toUser(JWTAuth::getToken())->restaurant_id;

        $driverPartners = DriverPartner::where('restaurant_id', $restaurant_id)->with('driver.user:id,profile_picture,user_name')->get();

        $response['status'] = true;
        $response['message'] = "Driver found successfully.";
        $response['data'] = $driverPartners;
        return response()->json($response);
    }

    public function deleteDriverPartner(Request $request) {
        $restaurant_id = JWTAuth::toUser(JWTAuth::getToken())->restaurant_id;

        $validator = Validator::make($request->all(), [
            'driver_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        }

        $driverPartner = DriverPartner::where('restaurant_id', $restaurant_id)->where('driver_id', $request->driver_id)->first();
        if($driverPartner) {
            $driverPartner->delete();
        }

        $driverPartners = DriverPartner::where('restaurant_id', $restaurant_id)->get();

        $response['status'] = true;
        $response['message'] = "Driver deleted successfully.";
        $response['data'] = $driverPartners;
        return response()->json($response);
    }


    public function getNearbyRestaurants(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'lat' => 'required',
            'lon' => 'required',
        ]);


        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        }

        $lat = $request->lat;
        $lon = $request->lon;

        $startDis = env('START_DIS','10');
        $endDis = env('END_DIS','100');

        $q = Restaurant::select('restaurants.*', DB::raw("( 3959 * acos( cos( radians(".$lat.") ) * cos( radians( O.lat ) )
                    * cos( radians( O.lon ) - radians(".$lon.") ) + sin( radians(".$lat.") ) * sin(radians(O.lat)) ) ) AS distance"));
        $q->join('users as O', 'O.id', '=', 'restaurants.user_id');
        $q->havingRaw('distance < '.$startDis)
            ->orderBy('distance', 'asc');
        $near_restaurants = $q->where('restaurants.status','!=',0)->get();

        $near_restaurants->each(function($restaurant, $key) use($lat, $lon,$near_restaurants) {
            list($distance, $time) = $restaurant->distanceTime($restaurant->id, $lat, $lon);
            $restaurant->distance_time = $time;
            $restaurant->distance = $distance;
            // $i = $key;
            // $namesArr =  Continental::whereIn('id',explode(",",$near_restaurants[$i]->continental))->pluck('name')->toArray();
            // $near_restaurants[$i]->continental_names = implode(',',$namesArr);
        });

        $response['status'] = true;
        $response['message'] = "Near by Restaurants.";
        $response['near_restaurants'] = $near_restaurants;

        return response()->json($response);
    }

}
