<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use JWTAuth;
use Hash;
use DB;
use App\Models\Category;
use App\Models\Item;
use App\Models\ItemImage;
use App\Models\Restaurant;

class ItemsController extends Controller
{

    public function getCategories(){
    	$categories = Category::select('id','title')->where('status',1)->get();
    	$response['status'] = true;
        $response['message'] = "categories list.";
        $response['data'] = $categories;
    	return response()->json($response);
    }

    public function getSubCategories(Request $request){
    	$validator = Validator::make($request->all(), [
            'category_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        }
    	$categories = Category::select('id','title')->where('status',1)->where('parent_id',$request->category_id)->get();

    	$response['status'] = true;
        $response['message'] = "sub categories list.";
        $response['data'] = $categories;
    	return response()->json($response);
    }

    public function addItem(Request $request) {
    	$userId = JWTAuth::toUser(JWTAuth::getToken())->id;
        $restaurant_id = JWTAuth::toUser(JWTAuth::getToken())->restaurant_id;

    	$validator = Validator::make($request->all(), [
            'item_title' => 'required',
            'category_id' => 'required',
            'item_price' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        }
        $discounted_price = $request->item_price;
        if(isset($request->coupon_code_percentage) && $request->coupon_code_percentage != null){
            $discounted_price = ($request->item_price-($request->item_price*$request->coupon_code_percentage)/100);
        }

        $item = new Item();
        $item->item_title = $request->item_title;
        $item->category_id = $request->category_id;
        $item->sub_category_id = $request->sub_category_id;
        $item->item_price = $request->item_price;
        $item->discounted_price = $discounted_price;
        $item->item_description = $request->item_description ?? '';
        $item->coupon_code = $request->coupon_code ?? 0;
        $item->coupon_code_percentage = $request->coupon_code_percentage ?? 0;
        $item->item_type = $request->item_type;
        $item->restaurant_id = $restaurant_id;
        $item->status = 1;
        $item->save();

        // Multi image upload.
        $files = $request->file('item_images');

        if($request->file('item_images'))
        {
            $destinationPath = storage_path('app/public/items/');
            foreach ($files as $file) {
                $name = 'item_'.$file->getSize().time().'.'.$file->extension();
                $file->move($destinationPath,$name);
                $itemImage = new ItemImage;
                $itemImage->item_id = $item->id;
                $itemImage->image = 'storage/app/public/items/'.$name;
                $itemImage->save();
            }
        }



        $items = Item::where('status','!=',0)->where('restaurant_id', $restaurant_id)->get();
        $items->each(function($item, $key) {
             $item->item_images;
        });

        $response['status'] = true;
        $response['message'] = "Item save successfully.";
        $response['data'] = $items;
    	return response()->json($response);
    }


    public function updateItem(Request $request) {
    	$userId = JWTAuth::toUser(JWTAuth::getToken())->id;
        $restaurant_id = JWTAuth::toUser(JWTAuth::getToken())->restaurant_id;
    	$validator = Validator::make($request->all(), [
            'item_title' => 'required',
            'category_id' => 'required',
            'item_price' => 'required',
            'item_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        }

        $discounted_price = $request->item_price;
        if(isset($request->coupon_code_percentage) && $request->coupon_code_percentage != null){
            $discounted_price = ($request->item_price-($request->item_price*$request->coupon_code_percentage)/100);
        }

        $item = Item::findOrFail($request->item_id);
        $item->item_title = $request->item_title;
        $item->category_id = $request->category_id ?? 0;
        $item->sub_category_id = $request->sub_category_id ?? 0;
        $item->item_price = $request->item_price ?? 0;
        $item->item_description = $request->item_description ?? '';
        $item->coupon_code = $request->coupon_code ?? 0;
        $item->coupon_code_percentage = $request->coupon_code_percentage ?? 0;
        $item->discounted_price = $discounted_price;
        $item->item_type = $request->item_type;
        $item->status = 1;
        $item->save();

        // Multi image upload.
        $files = $request->file('item_images');

        if($request->file('item_images'))
        {
            $destinationPath = storage_path('app/public/items/');
            foreach ($files as $file) {
                $name = 'item_'.$file->getSize().time().'.'.$file->getClientOriginalExtension();
                $file->move($destinationPath,$name);
                $itemImage = new ItemImage;
                $itemImage->item_id = $item->id;
                $itemImage->image = 'storage/app/public/items/'.$name;
                $itemImage->save();
            }
        }

        if($request->del_image_ids) {
        	$delFiles = (array) $request->del_image_ids;
        	$itemImages = ItemImage::whereIn('id', $delFiles)->get();
                foreach ($itemImages as $media) {
                    $vendorimg = str_replace('storage/', '', $media->image);
                    if (file_exists(storage_path($vendorimg))) {
                        unlink(storage_path($vendorimg));
                    }
                }
                ItemImage::whereIn('id', $delFiles)->delete();
        }



        $items = Item::where('status','!=',0)->where('restaurant_id', $restaurant_id)->get();
        $items->each(function($item, $key) {
             $item->item_images;
        });

        $response['status'] = true;
        $response['message'] = "Item updated successfully.";
        $response['data'] = $items;
    	return response()->json($response);
    }

    public function deleteItem(Request $request) {
        $restaurant_id = JWTAuth::toUser(JWTAuth::getToken())->restaurant_id;
    	$validator = Validator::make($request->all(), [
            'item_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        }

        $item = Item::findOrFail($request->item_id);
        $item->status = 0;
        $item->save();

        $items = Item::where('status','!=',0)->where('restaurant_id', $restaurant_id)->get();
        $items->each(function($item, $key) {
             $item->item_images;
        });

        $response['status'] = true;
        $response['message'] = "Item deleted successfully.";
        $response['data'] = $items;
    	return response()->json($response);
    }

    public function unavailableItem(Request $request) {
    	$validator = Validator::make($request->all(), [
            'item_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        }

        $item = Item::findOrFail($request->item_id);
        $item->status = 2;
        $item->save();

        $response['status'] = true;
        $response['message'] = "Item status changed successfully.";
        $response['data'] = $item;
    	return response()->json($response);
    }

    public function availableItem(Request $request) {
    	$validator = Validator::make($request->all(), [
            'item_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        }

        $item = Item::findOrFail($request->item_id);
        $item->status = 1;
        $item->save();

        $response['status'] = true;
        $response['message'] = "Item status changed successfully.";
        $response['data'] = $item;
    	return response()->json($response);
    }

    public function myListItem(Request $request) {
    	$userId = JWTAuth::toUser(JWTAuth::getToken())->id;
        $restaurant_id = JWTAuth::toUser(JWTAuth::getToken())->restaurant_id;

        $items = Item::where('status','!=',0)->where('restaurant_id', $restaurant_id)->get();
        $items->each(function($item, $key) {
             $item->item_images;
        });

        $response['status'] = true;
        $response['message'] = "Item list.";
        $response['data'] = $items;
    	return response()->json($response);
    }

    public function myCategoryWiseItem(Request $request) {
    	$userId = JWTAuth::toUser(JWTAuth::getToken())->id;
        $restaurant_id = JWTAuth::toUser(JWTAuth::getToken())->restaurant_id;

    	$validator = Validator::make($request->all(), [
            'category_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        }

        $items = Item::where('status','!=',0)->where('restaurant_id', $restaurant_id)->where('category_id', $request->category_id)->get();
        $items->each(function($item, $key) {
             $item->item_images;
        });
        $response['status'] = true;
        $response['message'] = "Item list.";
        $response['data'] = $items;
    	return response()->json($response);
    }


    public function itemDetail(Request $request) {
    	$userId = JWTAuth::toUser(JWTAuth::getToken())->id;

    	$validator = Validator::make($request->all(), [
            'item_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        }

        $item = Item::where('status','!=',0)->where('item_id', $request->item_id)->first();
        $item->item_images;

        $response['status'] = true;
        $response['message'] = "Item Detail.";
        $response['data'] = $item;
    	return response()->json($response);
    }

    public function categoryWiseItem(Request $request) {
        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;

        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'restaurant_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        }

        $items = Item::where('status','=',1)->where('restaurant_id', $request->restaurant_id)->where('category_id', $request->category_id)->get();
        foreach($items as $key => $item) {
            $cartItem = DB::table('carts')->join('cart_items','carts.id','=' ,'cart_items.cart_id')->where(['carts.user_id' => $userId,'carts.restaurant_id' => $request->restaurant_id,'cart_items.item_id' => $item->id])->first();
            if($cartItem) {
                $items[$key]['qty'] = $cartItem->qty;
            }
        }
        $response['status'] = true;
        $response['message'] = "Item list category wise.";
        $response['data'] = $items;
        return response()->json($response);
    }

    public function HomePageListItem(Request $request) {
        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;
        $restaurant_id = JWTAuth::toUser(JWTAuth::getToken())->restaurant_id;

        $validator = Validator::make($request->all(), [
            'lat' => 'required',
            'lon' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        }
        $lat = $request->lat;
        $lon = $request->lon;

        $startDis = env('START_DIS','10');
        $endDis = env('END_DIS','100');
        $q = Restaurant::select('restaurants.*', DB::raw("( 3959 * acos( cos( radians(".$lat.") ) * cos( radians( O.lat ) )
                    * cos( radians( O.lon ) - radians(".$lon.") ) + sin( radians(".$lat.") ) * sin(radians(O.lat)) ) ) AS distance"));
        $q->join('users as O', 'O.id', '=', 'restaurants.user_id');
        $q->havingRaw('distance < '.$startDis)
            ->orderBy('distance', 'asc');
        $popular_restaurants = $q->where('restaurants.status','!=',0)->get();

        $q = Item::select('items.*', DB::raw("( 3959 * acos( cos( radians(".$lat.") ) * cos( radians( O.lat ) )
                    * cos( radians( O.lon ) - radians(".$lon.") ) + sin( radians(".$lat.") ) * sin(radians(O.lat)) ) ) AS distance"));
        $q->join('restaurants as R', 'R.id', '=', 'items.restaurant_id');
        $q->join('users as O', 'O.id', '=', 'R.user_id');
        $q->where('items.status','=',1)->where('items.coupon_code','!=','0');
        $q->havingRaw('distance < '.$startDis)
            ->orderBy('distance', 'asc');
        $offer_dishes = $q->get();

        $q = Restaurant::select('restaurants.*', DB::raw("( 3959 * acos( cos( radians(".$lat.") ) * cos( radians( O.lat ) )
                    * cos( radians( O.lon ) - radians(".$lon.") ) + sin( radians(".$lat.") ) * sin(radians(O.lat)) ) ) AS distance"));
        $q->join('users as O', 'O.id', '=', 'restaurants.user_id');
        $q->havingRaw('distance < '.$startDis)
            ->orderBy('distance', 'asc');
        $near_restaurants = $q->where('restaurants.status','!=',0)->get();

        $near_restaurants->each(function($restaurant, $key) use($lat, $lon) {
            list($distance, $time) = $restaurant->distanceTime($restaurant->id, $lat, $lon);
            $restaurant->distance_time = $time;
            $restaurant->distance = $distance;
        });

        $response['status'] = true;
        $response['message'] = "Item list.";
        $response['popular_restaurants'] = $popular_restaurants;
        $response['offer_dishes'] = $offer_dishes;
        $response['near_restaurants'] = $near_restaurants;

        return response()->json($response);
    }


    public function getItemDetail(Request $request)
    {
        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;


        $validator = Validator::make($request->all(), [
            'restaurant_id' => 'required',
            'item_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        }

        $item_id = $request->item_id;
        $restaurant_id = $request->restaurant_id;
        $item = Item::where(['restaurant_id' => $restaurant_id,'id' => $item_id])->with('restaurant','item_images')->first();
        $cartItem = DB::table('carts')->join('cart_items','carts.id','=' ,'cart_items.cart_id')->where(['carts.user_id' => $userId,'carts.restaurant_id' => $request->restaurant_id,'cart_items.item_id' => $item_id])->get();
        if(!empty($cartItem) && isset($cartItem[0])) {
            $item['qty'] = $cartItem[0]->qty;
        }
        $response['status'] = true;
        $response['message'] = "Item Detail.";
        $response['data'] = $item;

        return response()->json($response);
    }

    public function getRestaurantCategoryVegItems(Request $request)
    {
        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;


        $validator = Validator::make($request->all(), [
            'restaurant_id' => 'required',
            'category_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        }

        $restaurant_id = $request->restaurant_id;
        $category_id = $request->category_id;
        $items = Item::where(['restaurant_id' => $restaurant_id,'category_id' => $category_id,'item_type' => 'Vegetarian'])->with('category')->get();

        foreach($items as $key => $item) {
            $cartItem = DB::table('carts')->join('cart_items','carts.id','=' ,'cart_items.cart_id')->where(['carts.user_id' => $userId,'carts.restaurant_id' => $request->restaurant_id,'cart_items.item_id' => $item->id])->first();
            if($cartItem) {
                $items[$key]['qty'] = $cartItem->qty;
            }
        }
        $response['status'] = true;
        $response['message'] = "Category Veg Items.";
        $response['data'] = $items;

        return response()->json($response);
    }
}
