<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use JWTAuth;
use Hash;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Cart;
use App\Models\CartItem;
use App\Models\OrderDelivery;
use App\Models\Restaurant;
use App\Models\Item;
use App\Events\AssignDriver;
use App\Models\Continental;
use App\Models\WalletTransaction;
use App\Models\Wallet;
use App\Models\GroupMember;
use App\Models\UserGroup;
use App\Models\WalletPercentage;
use App\Http\Controllers\Admin\UserFcmTokenController;
use App\Models\GroupCart;
use App\Models\GroupCartItem;
use App\Models\NearByDriverTemp;
use App\Models\Setting;
use App\Models\UserAddress;
use App\Models\UserGroupPoll;
use Exception;
use Svg\Tag\Group;
use Illuminate\Support\Facades\Log;

class OrdersController extends Controller
{
    public $fcm;

    public function __construct()
    {
        $this->api_per_page = config('constants.api_per_page');
        $this->fcm = new UserFcmTokenController;
    }
    public function testnotification()
    {
        $this->fcm->sendNotification(140,"Order Placed","Order placed successfully","order",0);
    }
    public function updateGroupMemberPaymentAmount($group_id,$userId,$grand_total) {
        $groupMember = GroupMember::where(['group_id'=>$group_id,'user_id'=>$userId])->first();
        if($groupMember) {
            // $groupMember->payment_status = "Paid";
            $groupMember->payment_amount = $grand_total;
            $groupMember->save();
        }
    }
    public function postItemInCart(Request $request) {

        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;
        // $userId = 178;
        $validator = Validator::make($request->all(), [
            'item_id' => 'required|numeric|min:1',
            'qty' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        }

        //Get Item detail
        $item = Item::where('id',$request->item_id)->first();
        if($item){

            //Create new entry for cart, if not exists
            if($request->group_id && $request->group_id > 0) {
                $cart = Cart::where(['user_id' => $userId,'group_id'=>$request->group_id])->first();
            }
            else {
                $cart = Cart::where(['user_id' => $userId])->first();
            }


            if(isset($cart) && $cart->restaurant_id != $item->restaurant_id) {
                return response()->json(['status'=>false,'message' => "Item belong from different restaurants.",'data' => $cart]);
            }
            if($request->group_id && $request->group_id > 0) {
                $cart = Cart::firstOrCreate(['restaurant_id'=> $item->restaurant_id, 'user_id' => $userId,'group_id'=>$request->group_id]);
            }
            else {
                $cart = Cart::firstOrCreate(['restaurant_id'=> $item->restaurant_id, 'user_id' => $userId]);
            }
                // echo "<pre>";
                // print_r($cart);
                // exit;
            //Creates new entry for cartItem, if not exists
            if($request->group_id && $request->group_id > 0) {
                $cartItem = CartItem::firstOrNew(['cart_id'=>$cart->id, 'item_id'=> $request->item_id,'member_id'=>$userId]);
            }
            else {
                $cartItem = CartItem::firstOrNew(['cart_id'=>$cart->id, 'item_id'=> $request->item_id]);
            }

            //Update the cart item details
            $cartItem->qty = $request->qty;
            $cartItem->price = $item->item_price;
            $cartItem->coupon_code_id = $item->coupon_code_id ?? null;
            $cartItem->sub_total = ($request->qty * $item->discounted_price);
            if($request->group_id && $request->group_id > 0)
            $cartItem->member_id = $userId;
            if($request->qty) {
            	$cartItem->save();
            } else {
            	$cartItem->delete();
            }

            //Get all cart items
            $cartItems = CartItem::where(['cart_id' => $cart->id])->get();
            $cartTotalAmt = 0;
            $cartGrandTotalAmt = $cartTotalAmtWithDiscount = 0;
            foreach($cartItems as $cartItem) {
                $cartTotalAmt += $cartItem->sub_total??0;
                $cartTotalAmtWithDiscount += ($cartItem->price*$cartItem->qty)??0;
            }

            //update the cart total
            $discount = $request->discount??0;
            $tax = $request->tax??0;

            $cartGrandTotalAmt = $cartTotalAmt + $tax - $discount;

            $cart->discount = $discount;
            $cart->tax = $tax;
            $cart->total_amount = $cartTotalAmtWithDiscount;
            $cart->grand_total = $cartGrandTotalAmt;

            $cart->save();
            if($request->group_id && $request->group_id > 0) {
                $this->updateGroupMemberPaymentAmount($request->group_id,$userId,$cart->grand_total);
            }
            $response['status'] = true;
            $response['message'] = "Item saved successfully.";
            $response['data'] = $cart;
        	return response()->json($response);
        }else{
            $response['status'] = false;
            $response['message'] = "This item is not available.";
            $response['data'] = [];
            return response()->json($response);
        }
    }
	public function cartDetail(Request $request) {
        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;

		$validator = Validator::make($request->all(), [
            'cart_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        }
        if($request->group_id && $request->group_id > 0) {
            $cart = Cart::where('user_id', $userId) ->where(['id' => $request->cart_id,'group_id' => $request->group_id]);

        }
        else {
            $cart = Cart::where('user_id', $userId)->where(['id' => $request->cart_id,'group_id' => 0]);
        }
        $cart = $cart->with('restaurant')
            ->first();
        if($cart) {

            $cart->cartItems;
            foreach($cart->cartItems as $key => $item) {
                $cart->cartItems[$key]->item = Item::where('id',$item->item_id)->first();
            }
            if($cart->restaurant && $cart->restaurant->continental) {

                $arr = explode(',',$cart->restaurant->continental);
                $continentals = Continental::whereIn('id',$arr)->pluck('name')->toArray();
                $cart->restaurant->continental_names = implode(',',$continentals);
            }
            if(count($cart->cartItems) > 0) {
                $response['status'] = true;
                $response['message'] = "Cart detail successfully.";
                $response['data'] = $cart;
                return response()->json($response);
            } else {
                $response['status'] = false;
                $response['message'] = "There is no item in cart.";
                $response['data'] = null;
                return response()->json($response);
            }
        }
        else {
            $response['status'] = false;
            $response['message'] = "There is no item in cart.";
            $response['data'] = null;
            return response()->json($response);
        }
	}

	public function emptyCart(Request $request){
		$userId = JWTAuth::toUser(JWTAuth::getToken())->id;

		$validator = Validator::make($request->all(), [
            'cart_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        }

        $cart = Cart::where('user_id', $userId)
        ->where('id', $request->cart_id)
        ->delete();
        CartItem::where('cart_id', $request->cart_id)->delete();

        $response['status'] = true;
        $response['message'] = "Your cart empty successfully.";
        $response['data'] = [];
    	return response()->json($response);
	}
    public function walletTransactions($userId,$stripe_token,$wallet_amount,$message,$transaction_type)
    {
            $wallet = Wallet::where(['user_id'=>$userId])->first();
            if($wallet)
                $wallet = $wallet;
            else
                $wallet = new Wallet;


            if($transaction_type=='debit')
            $amount = (float)($wallet->amount ?? 0) - (float)$wallet_amount;
            else if($transaction_type=='credit')
            $amount = (float)($wallet->amount ?? 0) + (float)$wallet_amount;


            $wallet->user_id = $userId;
            $wallet->amount = $amount;
            $wallet->save();

            $WalletTransaction = new WalletTransaction;

            $WalletTransaction->user_id = $userId;
            $WalletTransaction->wallet_id = $wallet->id;
            if($transaction_type=='debit')
            $WalletTransaction->paytype = -$wallet_amount;
            else
            $WalletTransaction->paytype = $wallet_amount;
            $WalletTransaction->stripe_token = $stripe_token;
            $WalletTransaction->comment = $message;
            $WalletTransaction->transaction_type = $transaction_type;
            $WalletTransaction->save();
    }
    public function getDriverDeliveryFee() {
        return WalletPercentage::where(['role_id'=>4])->first()->percentage ?? 0;
    }
    public function getOrderTax() {
        return Setting::where(['slug'=>'tax'])->first()->value ?? 0;
    }
    public function updateGroupStatus($group_id) {
        $group = UserGroup::where('id',$group_id)->with(['members'=>function($q) {
            $q->where('status','!=','Rejected');
        }])->first();
        if($group) {
            $group->status = "GROUP_CREATED";
            $group->save();

            UserGroupPoll::where(['group_id'=>$group->id,'creator_id'=>$group->creator_id])->delete();
            GroupMember::where('group_id',$group->id)->update(['payment_method'=>'','payment_amount'=>0,'payment_status'=>'']);


        }
    }
     public function cartGrandTotal($group_id,$user_id) {
        $discount = 0;
        $tax = 0;
        $deliveryFee = 0;
        $cartTotalAmt = 0;
        $cartGrandTotalAmt = 0;
        if($group_id > 0) {
            $carts = Cart::where('group_id', $group_id)->get();

            foreach($carts as $cart) {
                $cartIds[] = $cart->id;
                $cartItems = $cart->cartItems;

                if($cartItems) {

                    foreach ($cartItems as $key => $item) {
                        $itemDetail = Item::where('id',$item->item_id)->first();
                        if($itemDetail->coupon_code_percentage > 0) {
                            $discount += (($itemDetail->item_price * $itemDetail->coupon_code_percentage)/100);
                            $item->price = $itemDetail->item_price - (($itemDetail->item_price * $itemDetail->coupon_code_percentage)/100);
                        }

                        $sub_total = ($item->qty * $item->price);

                        $cartTotalAmt += $sub_total ?? 0;

                    }
                }
            }
        }
        else if($group_id == 0){
            $cart = Cart::where(['user_id'=> $user_id,'group_id'=>0])->first();

                $cartItems = $cart->cartItems;

                if($cartItems) {

                    foreach ($cartItems as $key => $item) {
                        $itemDetail = Item::where('id',$item->item_id)->first();
                        if($itemDetail->coupon_code_percentage > 0) {
                            $discount += (($itemDetail->item_price * $itemDetail->coupon_code_percentage)/100);
                            $item->price = $itemDetail->item_price - (($itemDetail->item_price * $itemDetail->coupon_code_percentage)/100);
                        }

                        $sub_total = ($item->qty * $item->price);

                        $cartTotalAmt += $sub_total ?? 0;

                    }
                }

        }
        if($this->getOrderTax() > 0) {
            $tax = (($cartTotalAmt * $this->getOrderTax())/100);
        }
        // echo $cartTotalAmt;
        // echo "<br>". $tax;
        // exit;
        if($this->getDriverDeliveryFee() > 0 && $cartTotalAmt > 0) {
            $deliveryFee = $this->getDriverDeliveryFee();
        }
        $cartGrandTotalAmt = $cartTotalAmt + $tax + $deliveryFee;

        return $cartGrandTotalAmt;
    }
    public function postOrder(Request $request) {
        // echo "<pre>";
        // print_r(json_decode($request->group_member_array));
//         if($request->group_member_array)
//         $this->update_group_member(json_decode($request->group_member_array),60,$request->user_group_id);
// exit;
        // exit;
        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;
    	$validator = Validator::make($request->all(), [
            'order_type' => 'required|in:Pickup,Delivery',
            'order_group_type' => 'required|in:Individual,Group',
            'cart_id' => 'required',
            'payment_mode' => 'required|in:wallet,cod'
        ]);


        if($request->user_group_id) {
            $validator = Validator::make($request->all(), [
                'user_group_id' => 'required|numeric|exists:user_groups,id',
            ]);
        }
        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        }
        $grand_total = $this->cartGrandTotal($request->user_group_id ?? 0,$userId);
        $wallet = Wallet::where('user_id',$userId)->first();
        if($wallet->amount < $grand_total) {
            return response()->json(['status'=>false,'message' => 'Your wallet is insufficient for make order please add more money to make order.']);
        }

        $userAddress = UserAddress::where(['user_id'=>$userId,'is_default'=>1])->first();
        if(!$userAddress){
            return response()->json(['status'=>false,'message' => 'Please select default address.']);
        }
        $discount = 0;
        $tax = 0;

        $cart = Cart::find($request->cart_id);

        $order = new Order();
        $order->user_id = $userId;
        $order->restaurant_id = $cart->restaurant_id;
        $order->discount = $cart->discount;
        $order->tax = $cart->tax;
        $order->total_amount = $cart->total_amount;
        $order->grand_total = $cart->grand_total;
        $order->special_instruction = $request->special_instruction ?? null;
        $order->order_type = $request->order_type;
        $order->order_group_type = $request->order_group_type;
        $order->user_group_id = $request->user_group_id ?? 0;
        $order->payment_type = $request->payment_mode ?? "";
        $order->active_order = 1;
        if($order->payment_type == 'wallet') {
            $order->payment_status = "Success";
        }
        $order->save();

        if($userAddress){
            $order->addOrderShippingAddresses($order->id,$order->user_id,$userAddress);
        }

        $user = $this->get_user_details($userId);

        // Check if order is group
        $cartTotalAmt = $cartTotalAmtWithDiscount = 0;
        $cartGrandTotalAmt = 0;
        $cartIds = [];
        if($request->user_group_id && $request->user_group_id > 0) {
            // echo "check";exit;

            $carts = Cart::where('group_id', $request->user_group_id)->get();

            foreach($carts as $cart) {
                $cartIds[] = $cart->id;
                $cartItems = $cart->cartItems;

                if($cartItems) {

                    foreach ($cartItems as $key => $item) {
                        $itemDetail = Item::where('id',$item->item_id)->first();

                        $orderItem = new OrderItem();
                        $orderItem->item_id = $item->item_id;
                        $orderItem->qty = $item->qty;
                        $orderItem->order_id = $order->id;
                        $orderItem->coupon_code_id = $item->coupon_code_id;
                        $orderItem->sub_total = $item->sub_total;
                        $itemPrice = $item->price;

                        if($itemDetail->coupon_code_percentage > 0) {
                            $discount += (($itemDetail->item_price * $itemDetail->coupon_code_percentage)/100)*$item->qty;
                            $item->price = $itemDetail->item_price - (($itemDetail->item_price * $itemDetail->coupon_code_percentage)/100);
                        }
                        $orderItem->price = $item->price;
                        $orderItem->sub_total = ($item->qty * $orderItem->price);
                        $orderItem->save();

                        $cartTotalAmt += $orderItem->sub_total??0;
                        $cartTotalAmtWithDiscount += ($item->qty*$itemPrice)??0;

                    }
                }
            }

        }
        else {
            $cartItems = $cart->cartItems;
            if($cartItems) {

                foreach ($cartItems as $key => $item) {
                    $itemDetail = Item::where('id',$item->item_id)->first();

                    $orderItem = new OrderItem();
                    $orderItem->item_id = $item->item_id;
                    $orderItem->qty = $item->qty;
                    $orderItem->order_id = $order->id;
                    $orderItem->coupon_code_id = $item->coupon_code_id;
                    $orderItem->sub_total = $item->sub_total;

                    $itemPrice = $item->price;

                    if($itemDetail->coupon_code_percentage > 0) {
                        $discount += (($itemDetail->item_price * $itemDetail->coupon_code_percentage)/100)*$item->qty;
                        $item->price = $itemDetail->item_price - (($itemDetail->item_price * $itemDetail->coupon_code_percentage)/100);
                    }
                     $orderItem->price = $item->price;
                     $orderItem->sub_total = ($item->qty * $orderItem->price);
                    $orderItem->save();
                    $cartTotalAmt += $orderItem->sub_total ?? 0;
                    $cartTotalAmtWithDiscount += ($item->qty*$itemPrice)??0;
                }
            }

        }
// print_r($cartIds) ;exit;
        //Get all cart items

        //update the cart total
        if($this->getOrderTax() > 0) {
            $tax = (($cartTotalAmt * $this->getOrderTax())/100);
        }


        $cartGrandTotalAmt = $cartTotalAmt + $tax;

        $order->discount = $discount;
        $order->tax = $tax;
        $order->total_amount = $cartTotalAmtWithDiscount;
        Log::info('This is total amount----.'.$cartTotalAmtWithDiscount);
        $order->grand_total = $cartGrandTotalAmt;
        
        if($order->order_type == 'Delivery') {
            Log::info('order type ----.'.'Delivery');
            $order->delivery_fee = $this->getDriverDeliveryFee();
        }
        else {
            Log::info('order type ----.'.'Else');
            $order->delivery_fee = 0;
        }
        if($request->order_type == 'Delivery' && $order->delivery_fee > 0) {
            $order->grand_total = $order->grand_total + $order->delivery_fee;
        }
        $order->save();

        // Delete cart after order insert
        if($request->user_group_id && $request->user_group_id > 0) {
            Cart::where('group_id', $request->user_group_id)->delete();

            CartItem::whereIn('cart_id', $cartIds)->delete();
            $this->updateGroupStatus($request->user_group_id);
        }
        else {
            $cart->delete();

            CartItem::where('cart_id', $request->cart_id)->delete();

        }
        $order->items;

        // debit money from wallet
        // if($request->order_group_type=='Individual')
        // {
            $with_msg = "$order->grand_total debited from your wallet by order id : $order->id";
            if(strtolower($request->payment_mode) != 'cod') {
                $this->walletTransactions($userId,$user->dwolla_customer_id ?? "",$order->grand_total,$with_msg,"debit");
            }
        // }


        // update group member
        // if($request->group_member_array && $request->order_group_type == 'Group') {
        //     $this->update_group_member(json_decode($request->group_member_array),$order->id,$request->user_group_id);
        // }

        $response['status'] = true;
        $response['message'] = "Order placed successfully.";
        $response['data'] = $order;

        $this->fcm->sendNotification($userId,"Order Placed",config('constants.order_place_user'),"order",$order->id ?? 0);
        $this->fcm->sendNotification($cart->restaurant_id,"Order Placed",config('constants.order_place_restuarant'),"order",$order->id ?? 0);
    	return response()->json($response);
    }

    public function get_user_details($user_id)
    {
        return User::find($user_id);
    }
    public function update_group_member($group_member_array,$order_id,$user_group_id)
    {
        if($group_member_array)
        {
            $total_wallet_amount = 0;
            $userGroup = UserGroup::where('id',$user_group_id)->first();
            foreach($group_member_array as $group)
            {
            $member = GroupMember::where(['user_id' => $group->user_id,'group_id' => $user_group_id])->first();
            if($member)
            {
                $member->payment_method = $group->payment_method;
                $member->payment_amount = $group->payment_amount;
                $member->save();
                if($group->payment_method=='Wallet' && $group->user_id != $userGroup->creator_id)
                {
                    $user = $this->get_user_details($group->user_id);
                    $with_msg = "$group->payment_amount debited from your wallet by order id : $order_id";
                    $this->walletTransactions($group->user_id,$user->dwolla_customer_id ?? "",$group->payment_amount,$with_msg,"debit");

                }
                else
                $total_wallet_amount += $group->payment_amount;


            }

            }



            $userAdmin = $this->get_user_details($userGroup->creator_id);

            $with_msg = "$total_wallet_amount debited from your wallet by order id : $order_id";
            $this->walletTransactions($userGroup->creator_id,$userAdmin->dwolla_customer_id ?? "",$total_wallet_amount,$with_msg,"debit");
        }
    }
    public function userOrderDecline(Request $request) {
        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;
        $validator = Validator::make($request->all(), [
            'order_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        }
        $order = Order::where(['id'=>$request->order_id,'user_id'=>$userId])->first();
        if($order->status == 'Processing') {
            $order->status = 'Decline';
            $order->save();

            if($order->save())
            {
                $message = config('constants.order_decline');
                if(strtolower($order->payment_type) != 'cod') {
                $this->order_decline_revert_money_to_wallet($request->order_id);
                }
                $response['status'] = true;
                $response['message'] = "Order declined successfully.";
                $response['data'] = $order;
                $this->fcm->sendNotification($userId,"Order Status Update",$message,"order",$order->id ?? 0);
                return response()->json($response);
            }
            else {
                $response['status'] = false;
                $response['message'] = "Order not declined.";
                $response['data'] = '';
            }
        }
        else {
            $response['status'] = false;
            $response['message'] = "Order not declined.";
            $response['data'] = '';
        }
        return response()->json($response);


    }
    public function changeOrderStatus(Request $request) {
        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;
        $validator = Validator::make($request->all(), [
            'order_status' => 'required',
            'order_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        }

        $order = Order::find($request->order_id);


        $order->status = $request->order_status;

        $order->save();


        if($request->order_status == 'Accept' && $order->order_type == 'Delivery') {
            $orderDelivery = OrderDelivery::firstOrCreate(['order_id'=>$request->order_id]);
            $order_id = $request->order_id;
            event(new AssignDriver($order_id));
            // Multiple Driver
            $driverArray = $order->findNearDriver($request->order_id);
            // echo "<pre>";
            // print_r($driverArray->toArray());die;
            if(count($driverArray) > 0) {
                $order->assignAllDriverRequest($driverArray,$order);
            }

        }
        $order = Order::find($request->order_id);

        $response['status'] = true;
        $response['message'] = "Order status changed successfully.";
        $response['data'] = $order;


            if($order->status == 'Accept')
            {
                $order->active_order = 1;
                $order->save();
                $message = config('constants.order_accept');
            }
            else if($order->status == 'Order In Kitchen')
            {
                $message = config('constants.order_in_kitchen');
                if($order->driver_id > 0) {
                    $this->fcm->sendNotification($order->driver_id,"Order Status Update",config('constants.order_in_kitchen_driver'),"order",$order->id ?? 0);
                }
            }
            else if($order->status == 'Decline')
            {
                $order->active_order = 0;
                $order->save();
                $message = config('constants.order_decline');
                if(strtolower($order->payment_type) != 'cod') {
                $this->order_decline_revert_money_to_wallet($request->order_id);
                }
            }
            else if($order->status == 'Processing'){
                $message = config('constants.order_processing');
            }
            else if($order->status == 'Ready To Pickup') {
                $message = config('constants.order_ready_to_pickup');
            }
            else if($order->status == 'Order Picked up'){
                $message = config('constants.order_ready_to_pickedup');
                if($order->order_type == 'Pickup') {
                    $order->active_order = 0;
                    $order->payment_status = "Success";
                    $order->save();
                    $this->credit_amount_admin_restaurant($request->order_id);
                }
            }

            // echo $userId. $message;dd();
            $this->fcm->sendNotification($userId,"Order Status Update",$message,"order",$order->id ?? 0);
        return response()->json($response);

    }


    public function getOrderList(Request $request) {
        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;
        $restaurant_id = JWTAuth::toUser(JWTAuth::getToken())->restaurant_id;
        $validator = Validator::make($request->all(), [
            'order_type' => 'required|in:Pickup,Delivery',
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        }

        $orderObj = Order::where('restaurant_id', $restaurant_id);

        if($request->order_type == 'Delivery') {
            $orderObj->where(function($query){
                $query->where('order_type','=','Delivery')
                    ->where('status','!=','Decline')->where('status','!=','Delivered');
            });
        }
        else if($request->order_type == 'Pickup') {
            $orderObj->where(function($query){
                $query->where('order_type','=','Pickup')
                    ->where('status','!=','Decline')
                    ->where('status','!=','Order Picked up');
            });
        }

        $orders = $orderObj->with('customer')
                    ->with('items')
                    ->with('driver')
                    ->with('shipping_address')
                    ->get();

        $response['status'] = true;
        $response['message'] = "Order list.";
        $response['data'] = $orders;
        return response()->json($response);
    }

    public function orderDetail(Request $request) {
        $validator = Validator::make($request->all(), [
            'order_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        }

        $order =  Order::where('id',$request->order_id)->with('items','user','driver','restaurant','shipping_address')->first();

        $response['status'] = true;
        $response['message'] = "Order detail.";
        $response['data'] = $order;
        return response()->json($response);
    }

    public function getOrderListInProcess(Request $request) {
        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;
        $restaurant_id = JWTAuth::toUser(JWTAuth::getToken())->restaurant_id;

        $status = ['Order In Kitchen', 'Ready To Pickup', 'Order Picked up'];
        $orders = Order::whereIn('status', $status)->where('restaurant_id', $restaurant_id)->get();

        $response['status'] = true;
        $response['message'] = "Order list.";
        $response['data'] = $orders;
        return response()->json($response);
    }

    public function getOrderListCompleted(Request $request) {
        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;
        $restaurant_id = JWTAuth::toUser(JWTAuth::getToken())->restaurant_id;

        $status = ['Delivered'];
        $orders = Order::whereIn('status', $status)->where('restaurant_id', $restaurant_id)->get();

        $response['status'] = true;
        $response['message'] = "Order list.";
        $response['data'] = $orders;
        return response()->json($response);
    }

    public function getOrderHistory(Request $request) {
        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;
        $restaurant_id = JWTAuth::toUser(JWTAuth::getToken())->restaurant_id;

        $orders = Order::where('user_id', $userId)->with('restaurant')->orderBy('id','desc')->get();

        $response['status'] = true;
        $response['message'] = "Order list.";
        $response['data'] = $orders;
        return response()->json($response);
    }

    public function getOrderListRequest(Request $request) {
        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;
        $driver_id = JWTAuth::toUser(JWTAuth::getToken())->driver_id;
        // $driver_id = $userId;

        $validator = Validator::make($request->all(), [
            'lat' => 'required',
            'lon' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        }
        $lat = $request->lat;
        $lon = $request->lon;

        $orders = Order::select('orders.*')
        ->join('order_deliveries as OD', 'OD.order_id', '=','orders.id')
        ->where('OD.driver_id', $driver_id)
        // ->where('OD.status', 'Processing')
        ->where('OD.status', 'Accept')
        ->where('orders.status','!=', 'Delivered')
        ->get();
        // echo "<pre>";
        // print_r($orders->toArray());exit;

        $orders->each(function($order, $key) use($lat, $lon) {
            $order->shipping_address;
            $order->restaurant;

            list($distance, $time) = $order->distanceTime($order->restaurant_id, $lat, $lon);
            $order->restaurant->distance_time = $time;
            $order->restaurant->distance = $distance;

            $order->item_count = OrderItem::where('order_id', $order->id)->count();

            $restaurant = Restaurant::select('U.lat','U.lon')->join('users as U', 'U.id','=','restaurants.user_id')->find($order->restaurant_id);
            $distance = $this->getDistance($restaurant->lat, $restaurant->lon, $order->shipping_address->lat, $order->shipping_address->lon);
            $order->shipping_address->distance = $distance;
            $order->shipping_address->customer = $order->customer;
        });

        $response['status'] = true;
        $response['message'] = "Order request list.";
        $response['data'] = $orders;
        return response()->json($response);
    }
    public function deleteAllDriverRequest($order_id){
        NearByDriverTemp::where('order_id',$order_id)->delete();
    }
    public function deleteParticularDriverRequest($order_id,$driver_id){
        NearByDriverTemp::where(['order_id'=>$order_id,'driver_id'=>$driver_id])->delete();
    }
    public function changeDeliveryOrderStatus(Request $request) {
        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;
        $driver_id = JWTAuth::toUser(JWTAuth::getToken())->driver_id ?? $userId;
        $driver_user_id = $userId;

        $validator = Validator::make($request->all(), [
            'order_status' => 'required',
            'order_id' => 'required',
            'lat' => 'required',
            'lon' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        }

        $lat = $request->lat;
        $lon = $request->lon;

        $orderDelivery = OrderDelivery::where('order_id', $request->order_id)->first();
        $order = Order::where('id', $request->order_id)->first();

        if($orderDelivery)
        {
            $orderDelivery->status = $request->order_status;

            $response['status'] = true;
            $response['message'] = "Order status changed successfully.";

            if($request->order_status == 'Accept')
            {

                $orderDelivery->driver_id = $driver_id;
                $orderDelivery->save();
                // Delete All driver request
                $this->deleteAllDriverRequest($request->order_id);



                $userId = $order->user_id;

                $order->driver_id = $driver_id;
                $order->save();



                $order->driver;
                $order->shipping_address;
                $order->restaurant;

                list($distance, $time) = $order->distanceTime($order->restaurant_id, $lat, $lon);
                $order->restaurant->distance_time = $time;
                $order->restaurant->distance = $distance;

                $order->item_count = OrderItem::where('order_id', $order->id)->count();
                // echo "tt ".$order->shipping_address->lat;exit;
                $restaurant = Restaurant::select('U.lat','U.lon')->join('users as U', 'U.id','=','restaurants.user_id')->find($order->restaurant_id);
                // print_r($order);exit;
                $distance = $this->getDistance($restaurant->lat, $restaurant->lon, $order->shipping_address->lat, $order->shipping_address->lon);

                $order->shipping_address->distance = $distance;
                $order->shipping_address->customer = $order->customer;
                $order->accept = 1;
                $response['data'] = $order;

                $driver = $this->get_user_details($driver_user_id);
                $message = "Driver $driver->user_name accepted order. Order id : $order->id";
                // $this->fcm->sendNotification($order->restaurant_id,"Order Status Update",$message,"order");


            }
            else if($request->order_status == 'Decline')
            {
                $order->status = $request->order_status;
                $order->active_order = 0;
                $order->save();
                $this->deleteParticularDriverRequest($request->order_id,$driver_id);
                $message = config('constants.order_decline');

            }
            else if($request->order_status == 'Delivered')
            {
                if($order->status == 'Order Picked up') {
                    $order->status = $request->order_status;
                    $order->active_order = 0;
                    $order->payment_status = "Success";
                    $order->save();
                    $message = config('constants.order_delivered');
                    $this->credit_amount_driver_admin_restaurant($request->order_id);
                }
                else {
                    $response['status'] = false;
                    $response['message'] = "Order status not picked up. you can't delivered this order.";
                    return response()->json($response);exit;

                }
            }


                $response['data'] = $order;

                $this->fcm->sendNotification($userId,"Order Status Update",$message,"order",$order->id ?? 0);
                $this->fcm->sendNotification($order->restaurant_id,"Order Status Update",$message,"order",$order->id ?? 0);

                return response()->json($response);
        } else {
            $response['status'] = false;
            $response['message'] = "Order not found.";
        }

        $order = new Order();
        $order->accept = null;
        $response['data'] = $order;
        return response()->json($response);

    }

    protected function credit_amount_driver_admin_restaurant($order_id)
    {
        $order = Order::find($order_id);

        $amount = $order->grand_total;

        $driver_id = $order->driver->user_id;
        if($driver_id)
        {
            $driver_money = (WalletPercentage::where(['role_id'=>4])->first()->percentage) ?? 0;
            // For driver wallet money credit after order deliver
            $driver = $this->get_user_details($driver_id);
            $with_msg = "$driver_money credit to your wallet by order id : $order->id";
            $this->walletTransactions($driver_id,$driver->dwolla_customer_id ?? "",$driver_money,$with_msg,"credit");
            // deduct amount
            $amount = $amount - $driver_money;
        }
        $amount = $amount - $order->tax;

        $admin_per = (WalletPercentage::where(['role_id'=>1])->first()->percentage) ?? 0;
        $admin_amount = ($amount * $admin_per) / 100;

        // For admin wallet percentage money credit after order deliver
        $admin_id = User::where(['is_admin'=>1])->first()->id;
        $admin = $this->get_user_details($admin_id);
        $with_msg = "$admin_amount credited to your wallet by order id : $order->id";
        $this->walletTransactions($admin_id,$admin->dwolla_customer_id ?? "",$admin_amount,$with_msg,"credit");

        // deduct amount
        $amount = $amount + $order->tax - $admin_amount;

        // For restaurant wallet money credit after order deliver
        $restaurant = Restaurant::select('U.id')->join('users as U', 'U.id','=','restaurants.user_id')->find($order->restaurant_id);
        $restaurant_id = $restaurant->id;

        $restaurant = $this->get_user_details($restaurant_id);
        $with_msg = "$amount credited to your wallet by order id : $order->id";
        $this->walletTransactions($restaurant_id,$restaurant->dwolla_customer_id ?? "",$amount,$with_msg,"credit");

    }
    protected function credit_amount_admin_restaurant($order_id)
    {
        $order = Order::find($order_id);

        $amount = $order->grand_total - $order->tax;
        $admin_per = (WalletPercentage::where(['role_id'=>1])->first()->percentage) ?? 0;

        $admin_amount = ($amount * $admin_per) / 100;

        // For admin wallet percentage money credit after order deliver
        $admin_id = User::where(['is_admin'=>1])->first()->id;
        $admin = $this->get_user_details($admin_id);
        $with_msg = "$admin_amount credited to your wallet by order id : $order->id";
        $this->walletTransactions($admin_id,$admin->dwolla_customer_id ?? "",$admin_amount,$with_msg,"credit");

        // deduct amount
        $amount = $amount + $order->tax - $admin_amount;

        // For restaurant wallet money credit after order deliver
        $restaurant = Restaurant::select('U.id')->join('users as U', 'U.id','=','restaurants.user_id')->find($order->restaurant_id);
        $restaurant_id = $restaurant->id;
        $restaurant = $this->get_user_details($restaurant_id);
        $with_msg = "$amount credited to your wallet by order id : $order->id";
        $this->walletTransactions($restaurant_id,$restaurant->dwolla_customer_id ?? "",$amount,$with_msg,"credit");

    }
    protected function order_decline_revert_money_to_wallet($order_id)
    {
        $order = Order::find($order_id);

        $amount = $order->grand_total;

        if($order->order_group_type == 'Individual')
        {

            $user_id = $order->user_id;
            $user = $this->get_user_details($user_id);
            $with_msg = "Order cancellation amount $amount credited to your wallet by order id : $order->id";
            $this->walletTransactions($user->id,$user->dwolla_customer_id ?? "",$amount,$with_msg,"credit");
        }
        else if($order->order_group_type == 'Group')
        {
            $group_members = GroupMember::where(['group_id'=>$order->user_group_id])->get();
            $total_wallet_amount = 0;
            foreach($group_members as $group)
            {
                if($group->payment_method == 'Wallet' && $group->user_id != $order->user_id)
                {
                    $user = $this->get_user_details($group->user_id);
                    $with_msg = "Order cancellation amount $group->payment_amount credited to your wallet by order id : $order->id";
                    $this->walletTransactions($user->id,$user->dwolla_customer_id ?? "",$group->payment_amount,$with_msg,"credit");
                }
                else {
                    $total_wallet_amount += $group->payment_amount;
                }

            }

            $user_id = $order->user_id;
            $user = $this->get_user_details($user_id);
            $with_msg = "Order cancellation $total_wallet_amount credited to your wallet by order id : $order->id";
            $this->walletTransactions($user_id,$user->dwolla_customer_id ?? "",$total_wallet_amount,$with_msg,"credit");
        }






    }





    public function clearCart() {
        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;
        try {
            $cart = Cart::where('user_id',$userId)->first();
            CartItem::where('cart_id',$cart->id)->delete();

            Cart::where('user_id',$userId)->delete();
            $response['status'] = true;
            $response['message'] = "Cart clear successfully.";
            return response()->json($response);
        }catch(Exception $e) {
            return response()->json(['status' => false,'message' => $e->getMessage()]);
        }
    }


    public function getCompleteOrders(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'restaurant_id' => 'required'
            ]);
            if ($validator->fails()) {
                return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
            }

            $orders = Order::where('restaurant_id',$request->restaurant_id);
            if($request->order_type === 'Pickup') {
                $orders->where('order_type','Pickup')
                    ->whereIn('status',['Order Picked up','Decline']);
            }elseif($request->order_type === 'Delivery') {
                $orders->where('order_type','Delivery')
                    ->whereIn('status',['Delivered','Decline']);
            }
            $orders = $orders->with('customer','items','shipping_address')->get();

            $response['status'] = true;
            $response['message'] = "Complete Order.";
            $response['data'] = $orders;
        }catch(Exception $e) {
            return response()->json(['status' => false,'message' => $e->getMessage()]);
        }
        return response()->json($response);
    }

    public function getPendingOrders(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'restaurant_id' => 'required',
                'order_type' => 'required|in:Delivery,Pickup'
            ]);
            if ($validator->fails()) {
                return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
            }

            $completedOrders = Order::where('restaurant_id',$request->restaurant_id);
            if($request->order_type === 'Pickup') {
                $completedOrders->where('order_type','Pickup')
                    ->where('status','!=','Delivered');
            }elseif($request->order_type === 'Delivery') {
                $completedOrders->where('order_type','Delivery')
                ->where('status','!=','Order Picked up');
            }
            $completedOrders = $completedOrders->pluck('id');

            $pendingOrders = Order::where('restaurant_id',$request->restaurant_id)->where('status','!=','Decline')->whereIn('id',$completedOrders)->with('customer','items','shipping_address')->get();
            $response['status'] = true;
            $response['message'] = "Pending Orders.";
            $response['data'] = $pendingOrders;
        }catch(Exception $e) {
            return response()->json(['status' => false,'message' => $e->getMessage()]);
        }
        return response()->json($response);
    }

    // Order History

    public function orderHistory(Request $request) {
        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;
        $restaurant_id = JWTAuth::toUser(JWTAuth::getToken())->restaurant_id;

        $orders = Order::where('user_id', $userId)->with('restaurant')->orderByDesc('id')->paginate($this->api_per_page);

        $response['status'] = true;
        $response['message'] = "Order list.";
        $response['data'] = $orders;
        return response()->json($response);
    }
    public function orderHistoryDriver(Request $request) {
        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;
        $driver_id = JWTAuth::toUser(JWTAuth::getToken())->driver_id ?? $userId;


        $orders = Order::where('driver_id', $driver_id)->with('user','restaurant')->orderByDesc('id')->paginate($this->api_per_page);

        $response['status'] = true;
        $response['message'] = "Order list.";
        $response['data'] = $orders;
        return response()->json($response);
    }
    public function orderDetails($id) {

        $order =  Order::where('id',$id)->with('items','user','driver','restaurant','shipping_address')->first();

        $response['status'] = true;
        $response['message'] = "Order detail.";
        $response['data'] = $order;
        return response()->json($response);
    }
    public function orderHistoryGroup(Request $request,$group_id) {
        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;

        $orders = Order::where('user_group_id', $group_id)->with('restaurant')->orderByDesc('id')->paginate($this->api_per_page);

        $response['status'] = true;
        $response['message'] = "Order list";
        $response['data'] = $orders;
        return response()->json($response);
    }
    public function activeOrder(Request $request,$user_id)
    {
      $order = Order::where(['user_id'=>$user_id,'active_order'=>1])->first();
      if($order) {
            $response['status'] = true;
            $response['message'] = "Active order detail";
            $response['data'] = $order;
        }
        else {
            $response['status'] = false;
            $response['message'] = "Order not found";
            $response['data'] = '';
        }
    return response()->json($response);
    }
    public function myActiveOrder(Request $request)
    {
        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;

        $orders = Order::where(['user_id'=>$userId,'active_order'=>1])->where('status','!=','Decline')->get();
        if($orders) {
                $response['status'] = true;
                $response['message'] = "Active order list";
                $response['data'] = $orders;
            }
            else {
                $response['status'] = false;
                $response['message'] = "Active order not found";
                $response['data'] = '';
            }
        return response()->json($response);
    }
    public function groupAdminCartDetails(Request $request) {
        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;

		$validator = Validator::make($request->all(), [
            'group_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->errors()->first()]);
        }

        $carts = Cart::where('group_id', $request->group_id)
        ->with('restaurant')
        ->get();
        if($carts) {
            $cart_array = [];
            foreach($carts as $cart) {
                $cart->cartItems;
                foreach($cart->cartItems as $key => $item) {
                    $cart->cartItems[$key]->item = Item::where('id',$item->item_id)->first();
                }
                if($cart->restaurant && $cart->restaurant->continental) {

                    $arr = explode(',',$cart->restaurant->continental);
                    $continentals = Continental::whereIn('id',$arr)->pluck('name')->toArray();
                    $cart->restaurant->continental_names = implode(',',$continentals);
                }
                $cart_array[] = $cart;
            }
        }

        $response['status'] = true;
        $response['message'] = "Cart detail successfully.";
        $response['data'] = $cart_array;
    	return response()->json($response);
	}


    ///////////////////////////////////////////////////////////////////////////////////////////
}
