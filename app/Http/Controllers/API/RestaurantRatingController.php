<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use JWTAuth;
use Hash;

use Exception;
use DB;

use App\Models\RestaurantRating;

class RestaurantRatingController extends Controller
{
    public function __construct()
    {
        $this->api_per_page = config('constants.api_per_page');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'restaurant_id' => 'required|numeric|min:1',
            'rating' => 'required|numeric|min:1|max:5',
            'review' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>"false",'message' => $validator->errors()->first()]);
        }

        $userId = JWTAuth::toUser(JWTAuth::getToken())->id;


        if($userId)
        {


            $restaurant_rating = RestaurantRating::where(['user_id'=>$userId,'restaurant_id'=>$request->restaurant_id])->first();
            if($restaurant_rating)
                $restaurant_rating = $restaurant_rating;
            else
                $restaurant_rating = new RestaurantRating;

            $restaurant_rating->user_id = $userId;
            $restaurant_rating->restaurant_id = $request->restaurant_id;
            $restaurant_rating->rating = $request->rating;
            $restaurant_rating->review = $request->review;
            $restaurant_rating->save();

            $response['status'] = true;
            $response['message'] = "Restaurant rating added successfully.";
            $response['data'] = $restaurant_rating;

            return response()->json($response);
        }
        else
        {
            $response['status'] = false;
            $response['message'] = "Restaurant rating not added.";
            $response['data'] = '';
            return response()->json($response);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RestaurantRating  $restaurantRating
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $restaurant_ratings = RestaurantRating::where(['restaurant_id'=>$id,'is_deleted'=>0])
                                ->with(['user','rastuarant'])->paginate($this->api_per_page);
        if($restaurant_ratings)
        {
            $response['status'] = true;
            $response['message'] = "Restaurant rating found successfully.";
            $response['data'] = $restaurant_ratings;

            return response()->json($response);
        }
        else
        {
            $response['status'] = false;
            $response['message'] = "Restaurant rating not found.";
            $response['data'] = '';
            return response()->json($response);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RestaurantRating  $restaurantRating
     * @return \Illuminate\Http\Response
     */
    public function edit(RestaurantRating $restaurantrating)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RestaurantRating  $restaurantRating
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RestaurantRating $restaurantRating)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RestaurantRating  $restaurantRating
     * @return \Illuminate\Http\Response
     */
    public function destroy(RestaurantRating $restaurantRating)
    {
        //
    }
}
