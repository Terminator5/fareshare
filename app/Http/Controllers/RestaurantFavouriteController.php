<?php

namespace App\Http\Controllers;

use App\RestaurantFavourite;
use Illuminate\Http\Request;

class RestaurantFavouriteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RestaurantFavourite  $restaurantFavourite
     * @return \Illuminate\Http\Response
     */
    public function show(RestaurantFavourite $restaurantFavourite)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RestaurantFavourite  $restaurantFavourite
     * @return \Illuminate\Http\Response
     */
    public function edit(RestaurantFavourite $restaurantFavourite)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RestaurantFavourite  $restaurantFavourite
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RestaurantFavourite $restaurantFavourite)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RestaurantFavourite  $restaurantFavourite
     * @return \Illuminate\Http\Response
     */
    public function destroy(RestaurantFavourite $restaurantFavourite)
    {
        //
    }
}
