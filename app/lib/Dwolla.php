<?php
namespace App\lib;

use App\Models\DwollaMaster;
use DwollaSwagger;
DwollaSwagger\Configuration::$username = config('constants.DWOLLA_KEY');
DwollaSwagger\Configuration::$password = config('constants.DWOLLA_SECRET');
// echo "in class";exit;

class Dwolla
{
    public $appClinet;
    public $tokensApi;
    public $appToken;
    public $dwollaClient;
    public $dwollaEnv;
    public $dwollaUrl;
    public function __construct()
    {
        $this->dwollaEnv = config('constants.DWOLLA_ENV');
        if($this->dwollaEnv == 'sandbox') {
            # For Sandbox
            $this->dwollaUrl = "https://api-sandbox.dwolla.com";
        }
        else if($this->dwollaEnv == 'production') {
            # For Production
            $this->dwollaUrl = "https://api.dwolla.com";
        }
        $this->apiClient = new DwollaSwagger\ApiClient($this->dwollaUrl);

        $this->tokensApi = new DwollaSwagger\TokensApi($this->apiClient);

        $this->appToken = $this->tokensApi->token();
        $this->appToken = $this->appToken['access_token'];

        DwollaSwagger\Configuration::$access_token = $this->appToken;

        $account_id = $this->root();
        $this->updateDwollaMasterAccountId($this->dwollaEnv,$account_id);
    }
    protected function updateDwollaMasterAccountId($dwollaEnv,$account_id) {
        $dwollaMaster = DwollaMaster::find(1);
        if($dwollaEnv == 'sandbox') {
            $dwollaMaster->sandbox_account_id = $account_id;
        }
        else {
            $dwollaMaster->production_account_id = $account_id;
        }
        $dwollaMaster->save();
    }
    public function root() {
        $rootApi = new DwollaSwagger\RootApi($this->apiClient);

        $root = $rootApi->root();
        $accountUrl = $root->_links["account"]->href;
        return  str_replace($this->dwollaUrl."/accounts/","",(string)$accountUrl);
        # => "https://api-sandbox.dwolla.com/accounts/ad5f2162-404a-4c4c-994e-6ab6c3a13254"


        // echo "<pre>";
        // print_r($root);

    }
    public function createPersonalVerifiedCustomer($user) {
        $customersApi = new \DwollaSwagger\CustomersApi($this->apiClient);

        // $location = $customersApi->create([
        //     'firstName' => 'Jennifer',
        //     'lastName' => 'Smith',
        //     'email' => 'text@gmail.com',
        //     'phone' => '7188675300'
        // ],
        // [
        //     'Idempotency-Key' => '9051a62-3403-11e6-ac61-9e71128cae77'
        // ]);

        $userNameFull = explode(" ",$user->user_name);
        if(count($userNameFull) > 1)
        {
            $firstName = $userNameFull[0];
            $lastName = $userNameFull[1];
        }
        else {
            $firstName = $userNameFull[0];
            $lastName = "NA";
        }

        $customer = $customersApi->create([
          'firstName' => $firstName,
          'lastName' => $lastName,
          'email' => $user->email,
          'type' => 'personal',
          'address1' => substr($user->address,0,50),
          'city' => $user->city,
          'state' => $user->state,
          'postalCode' => $user->zip,
          'dateOfBirth' => $user->dob ?? '1970-01-01',

          # For the first attempt, only the
          # last 4 digits of SSN required

          # If the entire SSN is provided,
          # it will still be accepted
          'ssn' =>  $user->ssn ?? '1234'
        ]);
        // https://api-sandbox.dwolla.com/customers/2d848257-baf4-4c42-ad0a-1c12f7a22c23

        $customer = str_replace($this->dwollaUrl."/customers/","",(string)$customer);
        return $customer;
        // echo "<pre>";
        // print_r($customer);
        // echo "Customer created successfully";
    }
    public function createVerifiedBusinessCustomer($user) {

      $customersApi = new \DwollaSwagger\CustomersApi($this->apiClient);

      $customer = $customersApi->create([
        'firstName' => $user->user_name,
        'lastName' => 'Owner',
        'email' => $user->email,
        'ipAddress' => '143.156.7.8',
        'type' => 'business',
        'dateOfBirth' => '1980-01-31',
        'ssn' => '6789',
        'address1' => $user->address,
        'city' => $user->city,
        'state' => $user->state,
        'postalCode' => $user->zip,
        'businessClassification' => '9ed3f670-7d6f-11e3-b1ce-5404a6144203',
        'businessType' => 'soleProprietorship',
        'businessName' => 'Jane Corp',
        'ein' => '00-0000000']);
        $customer = str_replace($this->dwollaUrl."/customers/","",(string)$customer);
        return $customer;
  }
  public function createReceiveOnlyCustomer($user) {
    $customersApi = new DwollaSwagger\CustomersApi($this->apiClient);

    $customer = $customersApi->create([
    'firstName' => $user->user_name,
    'lastName' => '',
    'email' => $user->email,
    'type' => 'receive-only',
    'ipAddress' => '99.99.99.99'
    ]);

    // print($customer); # => "https://api-sandbox.dwolla.com/customers/b81bd5d6-fa84-4355-9f5b-c50745f895b3"
    $customer = str_replace($this->dwollaUrl."/customers/","",(string)$customer);
    return $customer;
    }
    public function customerList() {
        $customersApi = new \DwollaSwagger\CustomersApi($this->apiClient);

        $customers = $customersApi->_list(10);
        // $customersApi->_list(limit, offset, search, status, headers, email)
        // $customers = $customersApi->_list(10, 0, null, null, [
        //     'Idempotency-Key' => '51a62-3403-11e6-ac61-9e71128cae77'
        // ], 'jsmith@gmail.com');

        echo "<pre>";
        print_r($customers->_embedded->customers);
    }
    public function generateIAVToken() {
      $customersApi = new \DwollaSwagger\CustomersApi($this->apiClient);
      $fsToken = $customersApi->getCustomerIavToken("https://api-sandbox.dwolla.com/customers/756e929e-262c-436d-8c4f-1847f3d2261a");
      return $fsToken->token; # => "8mpcN84QglQSkwcyr3jehMj9qkldNbrVo0UHtdyOES9xbcc7KP"
    }
    public function generatFundingSourceToken() {
      $customersApi = new \DwollaSwagger\CustomersApi($this->apiClient);
      $fsToken = $customersApi->getCustomerIavToken("https://api-sandbox.dwolla.com/customers/756e929e-262c-436d-8c4f-1847f3d2261a");
      return $fsToken->token; # => "8mpcN84QglQSkwcyr3jehMj9qkldNbrVo0UHtdyOES9xbcc7KP"
    }
    public function fundingAddSource(){
        $IAVToken = $this->generatFundingSourceToken();
        return view('funding-add',compact('IAVToken'));
    }
    public function retriveFundingSourceCustomers($dwolla_customer_id) {
        $customerUrl = $this->dwollaUrl.'/customers/'.$dwolla_customer_id;

        $fsApi = new DwollaSwagger\FundingsourcesApi($this->apiClient);

        $fundingSources = $fsApi->getCustomerFundingSources($customerUrl);
    //    echo  $fundingSources->_embedded->{'funding-sources'}[0]->name; # => "Jane Doe’s Checking"
        // echo "<pre>";
        // print_r($fundingSources);
        return $fundingSources->_embedded->{'funding-sources'};
    }
    public function retriveFundingSourceMaster() {
        $accountUrl = 'https://api-sandbox.dwolla.com/accounts/da037bc4-2099-459a-8884-bd8ee52cc1f0';

        $fsApi = new DwollaSwagger\FundingsourcesApi($this->apiClient);

        $fundingSources = $fsApi->getAccountFundingSources($accountUrl, $removed = false);
        # Access desired information in response object fields
        echo "<pre>";
        print_r($fundingSources->_embedded); # => PHP associative array of _embedded contents in schema
    }
    public function addMasterFundingSource() {
        $fundingApi = new DwollaSwagger\FundingsourcesApi($this->apiClient);

        $new_fs = $fundingApi->createCustomerFundingSource([
          "routingNumber" => "222222226",
          "accountNumber" => "123456789",
          "bankAccountType" => "checking",
          "name" => "Jane Merchant - Checking 6789"
          ], $this->dwollaUrl."/funding-sources"
        );

        print($new_fs);
    }
    public function addCustomerFundingSource($bankInfo) {
        $fundingApi = new DwollaSwagger\FundingsourcesApi($this->apiClient);

        $new_fs = $fundingApi->createCustomerFundingSource([
          "routingNumber" => $bankInfo['routingNumber'],
          "accountNumber" => $bankInfo['accountNumber'],
          "bankAccountType" => $bankInfo['bankAccountType'],
          "name" => $bankInfo['name']
          ], $this->dwollaUrl."/customers/".$bankInfo['dwolla_customer_id']
        );

        // print($new_fs); # => https://api-sandbox.dwolla.com/funding-sources/c26c2640-0fea-4956-b5a2-b4893c5013d0
        $customer = str_replace($this->dwollaUrl."/funding-sources/","",(string)$new_fs);
        return $customer;
    }
    public function removeCustomerFundingSource($id) {
        $fundingApi = new DwollaSwagger\FundingsourcesApi($this->apiClient);

        $fundingSourceUrl = $this->dwollaUrl.'/funding-sources/'.$id;

        $result = $fundingApi->softDelete(['removed' => true ], $fundingSourceUrl);
        // echo "<pre>";
        // print_r($result);
        // exit;
        return true;
    }
    public function sendMoneyToMaster($source_funding,$amount) {
        $master_account_funding = 'd8805cff-1485-4a51-8386-daf29017daa5';
        $transfer_request = array (
        '_links' =>
        array (
            'source' =>
            array (
            'href' => $this->dwollaUrl.'/funding-sources/'.$source_funding,
            ),
            'destination' =>
            array (
            'href' => $this->dwollaUrl.'/funding-sources/'.$master_account_funding,
            ),
        ),
        'amount' =>
        array (
            'currency' => config('constants.DWOLLA_CUR'),
            'value' => $amount,
        )
        );
    //     echo "<pre>";
    //     print_r($transfer_request); # => https://api-sandbox.dwolla.com/transfers/d76265cd-0951-e511-80da-0aa34a9b2388
    //    exit;
        $transferApi = new DwollaSwagger\TransfersApi($this->apiClient);
        $transfer = $transferApi->create($transfer_request);
    //     echo "<pre>";
    //     print_r($transfer_request); # => https://api-sandbox.dwolla.com/transfers/d76265cd-0951-e511-80da-0aa34a9b2388
    //    exit;
        $transfer = str_replace($this->dwollaUrl."/transfers/","",(string)$transfer);
        return $transfer;

    }
    public function sendMoneyToCustomer($destination_funding,$amount) {
        $master_account_funding = 'd8805cff-1485-4a51-8386-daf29017daa5';
        $transfer_request = array (
        '_links' =>
        array (
            'source' =>
            array (
            'href' => $this->dwollaUrl.'/funding-sources/'.$master_account_funding,
            ),
            'destination' =>
            array (
            'href' => $this->dwollaUrl.'/funding-sources/'.$destination_funding,
            ),
        ),
        'amount' =>
        array (
            'currency' => config('constants.DWOLLA_CUR'),
            'value' => $amount,
        )
        );

        // 'processingChannel' => [
        //     'destination' => 'real-time-payments'
        //   ]
        $transferApi = new DwollaSwagger\TransfersApi($this->apiClient);
        $transfer = $transferApi->create($transfer_request);
        // echo "<pre>";
        // print_r($transfer); # => https://api-sandbox.dwolla.com/transfers/d76265cd-0951-e511-80da-0aa34a9b2388
        $transfer = str_replace($this->dwollaUrl."/transfers/","",(string)$transfer);
        return $transfer;

    }
    public function masterAccountDetails() {
        // {
        //     "_links": {
        //         "self": {
        //             "href": "https://api-sandbox.dwolla.com/accounts/da037bc4-2099-459a-8884-bd8ee52cc1f0",
        //             "type": "application/vnd.dwolla.v1.hal+json",
        //             "resource-type": "account"
        //         },
        //         "receive": {
        //             "href": "https://api-sandbox.dwolla.com/transfers",
        //             "type": "application/vnd.dwolla.v1.hal+json",
        //             "resource-type": "transfer"
        //         },
        //         "funding-sources": {
        //             "href": "https://api-sandbox.dwolla.com/accounts/da037bc4-2099-459a-8884-bd8ee52cc1f0/funding-sources",
        //             "type": "application/vnd.dwolla.v1.hal+json",
        //             "resource-type": "funding-source"
        //         },
        //         "transfers": {
        //             "href": "https://api-sandbox.dwolla.com/accounts/da037bc4-2099-459a-8884-bd8ee52cc1f0/transfers",
        //             "type": "application/vnd.dwolla.v1.hal+json",
        //             "resource-type": "transfer"
        //         },
        //         "customers": {
        //             "href": "https://api-sandbox.dwolla.com/customers",
        //             "type": "application/vnd.dwolla.v1.hal+json",
        //             "resource-type": "customer"
        //         },
        //         "send": {
        //             "href": "https://api-sandbox.dwolla.com/transfers",
        //             "type": "application/vnd.dwolla.v1.hal+json",
        //             "resource-type": "transfer"
        //         }
        //     },
        //     "id": "da037bc4-2099-459a-8884-bd8ee52cc1f0",
        //     "name": "Nine hertz India",
        //     "timezoneOffset": -6.0,
        //     "type": "Commercial"
        // }
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
}
