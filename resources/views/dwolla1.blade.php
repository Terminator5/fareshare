<html>
<head></head>
<body>
<form accept-charset="UTF-8" action="https://uat.dwolla.com/payment/pay" method="post">
<input id="key" name="key" type="hidden" value="{{ $key }}" />
<input id="signature" name="signature" type="hidden" value="{{ $signature }}" />
<input id="timestamp" name="timestamp" type="hidden" value="{{ $timestamp }}" />
<input id="realtime" name="realtime" type="hidden" value="false" />
<input id="guestcheckout" name="guestcheckout" type="hidden" value="true" />
<input id="test" name="test" type="hidden" value="true" />
<input name="callback" type="hidden" value="{{ $callback_url }}" />
<input name="redirect" type="hidden" value="{{ $callback_url }}" />
<input id="destinationid" name="destinationid" type="hidden" value="812-167-9397" />
<input id="name" name="name" type="hidden" value="Purchase" />
<input id="description" name="description" type="hidden" value="Description" />
<table>
<tr>
<td>Order Id:</td>
<td><input id="orderid" name="orderid" value="{{ $order_id }}" /></td>
</tr>

<tr>
<td>Shipping Cost:</td>
<td><input id="shipping" name="shipping" value="1.00" /></td>
</tr>

<tr>
<td>Tax:</td>
<td><input id="tax" name="tax" value="2.00" /></td>
</tr>


<tr>
<td>Total Amount: </td>
<td><input id="amount" name="amount" value="1.00" /></td>
</tr>

<tr>
<td><button type="submit">Submit Order</button></td>
</tr>

</table>

</form>
</body>
</html>
