@extends('layouts.admin.login')
@section('content')


<div class="login-box">
  <div class="login-logo">
    <a href="javascript:void(0)"><b><?php echo env('APP_NAME') ?></b> | Retrieve </a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">You are only one step a way from your new password, recover your password now.</p>

      @if(Session::has('danger'))
         <div class="alert alert-danger text-center alert-dismissible">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Opps!</strong> {{ Session::get('danger') }}
         </div>   
      @endif

      @if(Session::has('success'))
         <div class="alert alert-success text-center alert-dismissible">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ Session::get('success') }}
         </div>   
      @endif

    @if ($errors->any())
	    <div class="alert absolte_alert alert-danger alert-dismissible" onClick= "this.remove();">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	        <ul>
	            @foreach($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif

      {!! Form::open() !!}
        
        <div class="input-group mb-3">
          
          <?= Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password', 'id' => 'inputPassword','required'=>true]); ?>

          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="hidden" name="token" value="<?php echo $_GET['token']; ?>" />

          <?= Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Confirm Password', 'id' => 'inputPassword','required'=>true]); ?>

          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <?= Form::submit('Update', ['class' => 'btn btn-primary btn-block']) ?>
          </div>
          <!-- /.col -->
        </div>
      {{ Form::close() }}

	
      <p class="mt-3 mb-1 ">
        <a href="{{ route('admin.login') }}"><strong>Login admin</strong></a>
        <span style="float: right;"><a href="{{ route('places.login') }}"><strong>Login Places</strong></a></span>
      </p>  
     
  
    
    </div>
    <!-- /.login-card-body -->
  	<br>
  </div>
    <br><br><br>
</div>
<!-- /.login-box -->

@endsection