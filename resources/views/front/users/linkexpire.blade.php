

<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>easyfreight</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 40px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .input-pass {
                border: 1px solid rgb(200,200,200);
                padding: 10px;
            }

            .button {
                background-color:#337ab7; 
                border-color:#2e6da4;
                /* background-color:#c4151c;  */
                color:#FFFFFF; 
                padding:10px 25px; 
                display:inline-block; 
                text-decoration:none; 
                margin-bottom:25px; 
                border-radius:5px;
            }

            .css-reset-password {
                padding: 30px 100px;
                border:1px solid rgb(220,220,220);
                border-radius:4px;
            }

        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height ">
            <div class="content ">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-8">
                            <div class="card">

                                <div class="card-body ">
                                    
                                        @csrf
                                        <h3 style="color:red;">
                                            <b>Link Expired</b>
                                        </h3>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
