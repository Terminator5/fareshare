@extends('layouts.login')

@section('content')
<div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">You forgot your password? Here you can easily retrieve a new password.</p>

      <form action="{{ route('password.email') }}" method="post">
         @csrf
         <div class="card-body">
            @include('admin.partials.alert_msg')
            <div class="form-group">
                <label for="email">Email</label>
                <input id="email" type="email" placeholder="Email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                @error('email')
                    <span class="invalid-feedback">{{ $message }}</span>
                @enderror
            </div>
          </div>
      <div class="row">
          <!-- /.col -->
          <div class="col-12">
            <button type="submit" class="btn btn-primary btn-block">Send Password Reset Link</button>
        </div>
        <p class="mb-1">
          <a href="{{ route('login') }}">Login</a>
      </p>
      <!-- /.col -->
  </div>
</form>

</div>
<!-- /.login-card-body -->
</div>

@endsection
