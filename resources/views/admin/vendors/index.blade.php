@extends('layouts.admin')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">{{ $page_title }}</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
              <li class="breadcrumb-item active">{{ $page_title }}</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">{{ $page_title }}</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="userTable" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th width="5%">S.NO</th>
                  <th>Email</th>
                  <th>Username</th>
                  <th>Mobile</th>
                  <th>City</th>
                  <th>State</th>
                  {{-- <th>Country</th> --}}
                  <!-- <th>Address</th> -->
                  <th>Created</th>
                  <th width="5%">Status</th>
                  <th width="10%">Action</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('styles')
<!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('public/admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('public/admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
  <link data-require="sweet-alert@*" data-semver="0.4.2" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />
@endsection

@section('scripts')
<!-- Bootstrap 4 -->
<script src="{{ asset('public/admin/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- DataTables -->
<script src="{{ asset('public/admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('public/admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('public/admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript">
$(document).ready(function () {
  
  $('#userTable').DataTable({
        "bProcessing": true,
        "serverSide": true,
        "ajax":{
            url :"{{ url('admin/vendor-list') }}",
            
            error: function(){  
                alert('Something went wrong');
            }
        },
        "aoColumns": [
            { mData: 'id', orderable: false,
            render: function (data, type, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }},
            { mData: 'email' },
            { mData: 'user_name' },
            { mData: 'mobile_number' },
            { mData: 'city' },
            { mData: 'state' },
            // { mData: 'country' },
            // { mData: 'address' },
            { mData: 'created_at', orderable: false },
            { mData: 'status', orderable: false },
            { mData: 'actions', orderable: false },
        ],
        "aoColumnDefs": [
          { "bSortable": false, "aTargets": ['action'] }
        ]
    });

  /** Active Admin **/
        $("#userTable").on('click','.active-button',function(){
            user_id = $(this).attr('data-id');
            swal({
              title: "Are you sure you want to active this?",
              icon: "success",
              buttons: [
                'No, cancel it!',
                'Yes, I am sure!'
              ],
              dangerMode: false,
            }).then(function(isConfirm) {
              if (isConfirm) {
                $.ajax({
                       url:"{{ url('/admin/vendor-active') }}" +"/"+user_id,
                       success:function(data)
                       {
                        setTimeout(function(){
                         $('#userTable').DataTable().ajax.reload();
                        }, 1000);
                       }
                      });
              } 
            });
        });
        

        /** Inactive Admin **/
        $("#userTable").on('click','.inactive-button',function(){
            user_id = $(this).attr('data-id');
            // alert(user_id)
            swal({
              title: "Are you sure you want to inactive this?",
              icon: "warning",
              buttons: [
                'No, cancel it!',
                'Yes, I am sure!'
              ],
              dangerMode: false,
            }).then(function(isConfirm) {
              if (isConfirm) {
                $.ajax({
                       url:"{{ url('admin/vendor-inactive') }}" +"/"+user_id,
                       success:function(data)
                       {
                        setTimeout(function(){
                         $('#userTable').DataTable().ajax.reload();
                        }, 1000);
                       }
                      });
              } 
            });
        });
  
});
</script>
@endsection
