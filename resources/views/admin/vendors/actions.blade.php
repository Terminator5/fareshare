<a class="btn btn-info btn-xs" href="{{ url('admin/vendor-agreement',$id)}}" data-toggle="tooltip" title="Agreement" >
<i class="far fa-file-pdf"></i>
</a>
<a class="btn btn-primary btn-xs" href="{{ url('admin/vendor-detail',$id)}}" data-toggle="tooltip" title="view" >
<i class="far fa-eye"></i>
</a>

@if($status == 0)
<a class="btn btn-success btn-xs active-button" href="#" data-id="{{ $id }}">
  Active
</a>

@else
<a class="btn btn-danger btn-xs inactive-button" href="#" data-id="{{ $id }}">
  In Active
</a>
@endif
