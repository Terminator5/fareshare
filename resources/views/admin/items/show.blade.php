@extends('layouts.admin')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">{{ $page_title }}</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
              <li class="breadcrumb-item active">{{ $page_title }}</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    @php
    function dateFormat($date)
    {
        return date("d-M-Y",strtotime($date));
    }
    @endphp
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">{{ $page_title }}</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              @include('admin.partials.alert_msg')
              <div class="table-responsive">
                <table class="table table-bordered table-hover">
                  <tr>
                      <th>ID</th><td>{{$item->id}}</td>
                      <th>Item Title</th><td>{{ucwords($item->item_title)}}</td>
                  </tr>

                  <tr>
                      <th>Category</th><td>{{$item->category_title}}</td>
                      <th>Sub Category</th><td>{{$item->sub_category_title}}</td>
                  </tr>
                  <tr>
                      <th>Item Price</th><td>{{$item->item_price}}</td>
                      <th>Item Description</th><td>{{$item->item_description}}</td>
                  </tr>
                  <tr>
                    <th>Coupon Code</th><td>{{$item->coupon_code}}</td>
                    <th>Coupon Code Percentage</th><td>{{$item->coupon_code_percentage}}</td>
                  </tr>
                    <tr>
                      <th>Item Type</th><td>{{$item->item_type}}</td>
                      <th>Status</th><td>@if($item->status == 1)<span class="text-success">Active</span>@else<span class="text-danger">Inactive</span>@endif</td>
                    </tr>
                    <tr>
                      <th>Restaurant</th><td>{{ucwords($item->restaurant->restaurant_name ?? "")}}</td>
                      <th>Created At</th><td>{{dateFormat($item->created_at)}}</td>
                    </tr>
                  </tr>
                </table>
                </div>


            </div>
          </div>
        </div>



         <!-- /.row -->










      </div>
    </section>
    <!-- /.content -->
  </div>
@endsection



@section('scripts')


<script type="text/javascript">
$(document).ready(function () {





});
</script>
@endsection
