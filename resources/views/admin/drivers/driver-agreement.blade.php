<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>{{ $page_title }}</title>


<!-- Theme style -->
<!-- <link rel="stylesheet" href="{{ asset('/admin/css/adminlte.min.css') }}"> -->


</head>

<body>



  <!-- Page Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h3 align="center" class="text-center"> {{ $page_title }}</h3>

<p>Name : <u>{{$user->user_name}}</u></p>
<p>Full Legal Address : {{$user->address}} {{$user->city}} {{$user->state}} {{$user->country}} {{$user->zip}}</p>

<p>This Driver Agreement (the “Agreement”) is entered into as of _________ __, 20____ (the “Effective Date”) by and between _________, a _______ [individual, corporation/limited liability company/partnership/etc.] (the “Driver” or “You”), and Fair Share, LLC, a limited liability company (the “Company,” and together with the Driver referred to as the “Parties”).</p>

<h3 align="center" class="text-center">ABOUT THE FAIR SHARE PLATFORM</h3>

<p>Company has created an app based platform which connects its paying Users to available Drivers for certain services.</p>

<p>The Fair Share platform provides a service allowing its Users to order food and beverages from certain affiliated restaurants or “Vendors” and have their order delivered to them by a Fair Share Driver.   As a Driver you will be matched with a User through the platform. You will have the ability to accept or decline a delivery request with certain restrictions. If you choose to accept the Delivery request then you will be provided with pertinent information such as User name, Vendor pick up location and pick up time, Order Number if applicable, Delivery location.</p>

<p>The platform features a Driver “Wallet” wherein your payments are maintained until such time as you choose to cash it out.</p>

<p>Whereas the Driver wishes to perform services to the Company and the Company wishes to pay the Driver for these services, and for other good consideration, the Parties hereby agree as follows:</p>

<h3>1. ENGAGEMENT SERVICES</h3>

<h4>a. Engagement</h4> <p>The Company retains the Driver to provide, and the Driver shall provide, the following services: Once a Delivery Job is offered to and accepted by the Driver via the Fair Share app, then the Driver shall promptly arrive at the designated Vendor’s location within the time frame provided, provide the necessary Order information to the Vendor, pick up the Order and promptly deliver same to the designated User.</p>

<h4>b. Services.</h4><p> Without limiting the scope of Services described above, the Driver shall:</p>

<p>(i) perform the Services set forth in <b>1(a)</b> above;</p>

<p>(ii) devote as much productive time, energy, and ability to the performance of their duties under this agreement as may be necessary to provide the required Services in a timely manner;</p>

<p>(iii) perform the Services in a safe, good, and workmanlike manner maintaining a professional demeanor at all times and agreeing to abide by local traffic law;</p>

<p>(iv) communicate with the Company about any issues with the app, Vendors or Users that they may encounter;</p>

<p>(v) supply their own automobile and maintain liability insurance in the policy limits required by law;</p>

<p>(vi) provide Services  that are satisfactory and acceptable to the Company;</p>

<h3>2.	COMPENSATION</h3>	<p>You will be compensated in the form of tips to be paid by the Users.  Users will be given the opportunity to add a tip based on percentage through the app as well prior to Delivery.  The default tip amount will be up to 15%. No other compensation will be given.</p>
<p>Changes to compensation are subject to change without notice please review periodically any updates on our main FairShare, LLC website under the employee handbook manual link.</p>

<h4>(a)	No Payments in Certain Circumstances.</h4><p> No payment will be payable to the Driver under any of the following circumstances:</p>

<p>A. if prohibited under applicable government law, regulation, or policy;</p>

<p>B. if the Driver did not directly perform or complete the Services described in <b>1(a)</b>;</p>

<p>C. if the Driver did not perform the Services to the reasonable satisfaction of the Company; or</p>

<p>D. if the Services performed occurred after the expiration or termination of the Term, unless otherwise agreed in writing.</p>

<h4>c. No Other Compensation.</h4> <p>The compensation set out above will be the Driver’s sole compensation under this agreement.</p>

<h3> Expenses</h3><p> Any ordinary and necessary expenses incurred by the Driver or their staff in the performance of this agreement will be the Driver’s sole responsibility.</p>
<h4>e. Background Check.</h4><p> Prior to any driver being able to perform actions which would reflect He/She as an independent contractor under Fare Share, all drivers are required to undergo a background check. The cost for this service per driver is in the amount of 10.00 which should be paid at the time of application through the Fare Share website (Fareshare.info.). Applications should be completed through the Fare Share app which can be found in the Google Play app Store and or the Apple store. The fee for the background check at the time of application is nonrefundable. Any offense that is consonsider any potential risk will be at the consideration of the Admin and at the Admins discretion solely.</p>

<h4>f. Taxes.</h4><p> The Driver is solely responsible for the payment of all income, social security, employment-related, or other taxes incurred as a result of the performance of the Services by the Driver under this agreement, and for all obligations, reports, and timely notifications relating to those taxes. The Company has no obligation to pay or withhold any sums for those taxes.</p>

<h4>g. Other Benefits</h4><p> The Driver has no claim against the Company under this agreement or otherwise for vacation pay, sick leave, retirement benefits, social security, worker’s compensation, health or disability benefits, unemployment insurance benefits, or employee benefits of any kind.</p>


<h3>3.	PAYMENTS</h3><p>	Company will transfer your earned Fare and any additional tips to your “Wallet” featured on the App within   ____ (days/hours) after User has confirmed that Delivery has been made. Under no circumstances will Driver be compensated until Company receives payment from Users.</p>

<h3>4.	CANCELLATIONS</h3><p>	If the User cancels their Order prior to delivery then no compensation will be earned.</p>

<h3>5.	TERM AND TERMINATION</h3>

<h4>a. Term</h4><p> This agreement will become effective once it is executed by Driver. This agreement will continue until the Services have been satisfactorily completed and the Contractor has been paid in full for such Services.</p>

<h4>b. Termination</h4><p> This agreement may be terminated:</p>

	<p>(1) by either party on provision of 3 days’ written notice to the other party, with or without cause;</p>

	<p>(2)	by either party for a material breach of any provision of this agreement by the other party, if the other party’s material breach is not cured within 3 days of receipt of written notice of the breach;</p>

	<p>(3)	automatically, on the death of the Contractor.</p>

<h4>c. Effect of Termination</h4><p> After the termination of this agreement for any reason, the Company shall promptly pay the Contractor for Services rendered before the effective date of the termination. No other compensation, of any nature or type, will be payable after the termination of this agreement.</p>






<h3>6.	NATURE OF RELATIONSHIP</h3>

<h4>h. Independent Contractor Status</h4>

<p>(a) The relationship of the parties under this agreement is one of independent Contractors, and no joint venture, partnership, agency, employer-employee, or similar relationship is created in or by this agreement. Neither party may assume or create obligations on the other party’s behalf, and neither party may take any action that creates the appearance of such authority.</p>

<p>(b) The Driver has the right to accept or refuse Delivery Jobs in their sole discretion.</p>

<p>(c) The Driver will be responsible for their own transportation to and from Delivery Jobs.</p>


<h3>7.	INDEMNIFICATION</h3>

<h4>i. Of Company by Driver. </h4><p>At all times after the effective date of this agreement, the Contractor shall indemnify the Company and its members, managers, owners, successors, and assigns (collectively, the “Company Indemnitees”) from all damages, liabilities, expenses, claims, or judgments (including interest, penalties, reasonable attorneys’ fees, accounting fees, and expert witness fees) (collectively, the “Claims”) that any Company Indemnitee may incur and that arise from:</p>

<p>A. the Driver’s gross negligence or willful misconduct arising from the Contractor’s carrying out of their obligations under this agreement;</p>

<p>B. the Driver’s breach of any of their obligations or representations under this agreement; or</p>

<p>C. the Driver’s breach of their express representation that he/she is an independent contractor and in compliance with all applicable laws related to work as an independent contractor. If a regulatory body or court of competent jurisdiction finds that the Driver is not an independent contractor or is not in compliance with applicable laws related to work as an independent contractor, based on the Driver’s own actions, the Driver will assume full responsibility and liability for all taxes, assessments, and penalties imposed against the Driver or the Company resulting from that contrary interpretation, including taxes, assessments, and penalties that would have been deducted from the Driver’s earnings if the Driver had been on the Company’s payroll and employed as a Company employee.</p>



<h3>8.	ASSIGNMENT</h3>

<p>Neither party may assign this agreement or delegate any of their duties to any third party without the written consent of the other party.</p>

<h3>9.	SUCCESSORS AND ASSIGNS</h3>

<p>All references in this Agreement to the Parties shall be deemed to include, as applicable, a reference to their respective successors and assigns. The provisions of this Agreement shall be binding on and shall insure to the benefit of the successors and assigns of the Parties.</p>

<h3>10.	 CHOICE OF LAW</h3>

<p>The parties agree that the laws of the State of New Jersey shall govern any dispute relating to this Agreement and further agree to submit themselves to the exclusive jurisdiction of the Courts of the State of New Jersey with regard to the adjudication of any disputes resulting therefrom.</p>


<h3>SIGNATURE AND DATE</h3>

<p>Signature : @if($user->sign)<img src="{{$user->sign}}" width="100" height="100"/>@endif</p>
<p>Date : {{ date('d/m/Y', strtotime($user->created_at)) }}  </p>

      </div>
    </div>
  </div>

 <!-- jQuery -->
<!-- <script src="{{ asset('/admin/plugins/jquery/jquery.min.js') }}"></script> -->

<!-- AdminLTE App -->
<!-- <script src="{{ asset('/admin/js/adminlte.js') }}"></script> -->

</body>

</html>
