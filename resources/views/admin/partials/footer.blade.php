<footer class="main-footer">
	<strong>Copyright &copy; 2020 <a href="{{ url('/') }}">{{ config('app.name', 'Laravel') }}</a>.</strong>
	All rights reserved.
</footer>