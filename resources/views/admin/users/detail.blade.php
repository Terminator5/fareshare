@extends('layouts.admin')

@section('content')


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">{{ $page_title }}</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
              <li class="breadcrumb-item active">{{ $page_title }}</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">{{ $page_title }} #{{ $user->user_name }}</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div class="row">
                <!-- /.col -->
                <div class="col-12">
                  <div class="table-responsive">
                    <table class="table">
                      <tr>
                        <th style="width:25%">Username:</th>
                        <td>{{ ucfirst($user->user_name) }}</td>
                      </tr>
                      <tr>
                        <th>Email</th>
                        <td>{{ $user->email }}</td>
                      </tr>
                      <tr>
                        <th>Mobile No.:</th>
                        <td>+{{ $user->country_code }}-{{ $user->mobile_number }}</td>
                      </tr>
                      <tr>
                        <th>Dob:</th>
                        <td>{{ date("d-m-Y",strtotime($user->dob)) }}</td>
                      </tr>
                      <tr>
                        <th>SSN:</th>
                        <td>{{ $user->ssn }}</td>
                      </tr>
                      <tr>
                        <th>Address:</th>
                        <td>{{ $user->address }}</td>
                      </tr>
                      <tr>
                        <th>State:</th>
                        <td>{{ $user->state }}</td>
                      </tr>
                      <tr>
                        <th>City:</th>
                        <td>{{ $user->city }}</td>
                      </tr>
                      <tr>
                        <th>Country:</th>
                        <td>{{ $user->country }}</td>
                      </tr>
                      <tr>
                        <th>Zip:</th>
                        <td>{{ $user->zip }}</td>
                      </tr>
                      <tr>
                        <th>Profile Picture:</th>
                        <td>
                          @if($user->profile_picture)
                          <div class="widget-user-image">
                            <img class="img-circle elevation-2 img-lg" src="{{ asset($user->profile_picture) }}" alt="{{ $user->user_name }}">
                          </div>
                          @endif
                        </td>
                      </tr>
                      <tr>
                        <th>Status:</th>
                        <td>
                          @if($user->status == 1)
                          <span class="badge badge-success">Active</span>
                          @else
                          <span class="badge badge-danger">In Active</span>
                          @endif
                        </td>
                      </tr>
                    </table>
                  </div>
                </div>
                <!-- /.col -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('styles')

@endsection

@section('scripts')

<script type="text/javascript">
$(document).ready(function () {



});
</script>
@endsection
