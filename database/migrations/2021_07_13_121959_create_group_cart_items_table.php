<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupCartItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_cart_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('group_cart_id')->default(0);
            $table->bigInteger('item_id')->default(0);
            $table->bigInteger('coupon_code_id')->default(0);
            $table->integer('qty')->default(0);
            $table->float('price')->default(0);
            $table->float('sub_total')->default(0);
            $table->bigInteger('member_id')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_cart_items');
    }
}
